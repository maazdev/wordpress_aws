<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'creativefund');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'inv12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_MEMORY_LIMIT', '64M');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@{js-Xgfi;*/bebbuvFLEw_0o4ane<S1*R~iDtZwm_6)>:).RH+0K:BX~(66EEn:');
define('SECURE_AUTH_KEY',  'uM:1t=*7O*s}W]/_!b+ _W[7LA|Y]I3:2vCHjnx^|TR@jiPCaB.zI{H^fpG3H[Rt');
define('LOGGED_IN_KEY',    'som2TB_CKH/Wo_+)=,olC!m}rP.]BICDaa+xdga2MYD/[H3kpI% Yz&vIrA][t4$');
define('NONCE_KEY',        '._[~_HB~_u_P?8pl&g0V$Xr&H8#dIv=yr(O.mh]LCh` jN&>24ACD+ciWw5Md2@j');
define('AUTH_SALT',        'OFc>aZ}0:Z`#[S=h!pNUFu}gWQFh ypx|TL|q~*}yS+qk,h.#aXE](`_k-Oy:}K<');
define('SECURE_AUTH_SALT', 'DC$l]:T<cU]}xK@~yzHY}h.f3jX:=JXE>`3zGIy Kjd6.~q.!LpiGTe+~n9L`;VX');
define('LOGGED_IN_SALT',   '{UcUS`ab|5w=s8?K($(3mOLkb*Xf?;p(t2bSra)pJu4B[I~,jGXIa09;SlJ!m{yA');
define('NONCE_SALT',       'D5)GGkjoPwH#C9m{!g8NyR:Ds[1l3fH-x$<dB.KTS5HO- TOQio+~sZr4$Fa_`Gu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
