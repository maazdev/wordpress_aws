<?php

if (!function_exists('sh_set')) {

    function sh_set($var, $key, $def = '') {
        if (!$var)
            return false;
        if (is_object($var) && isset($var->$key))
            return $var->$key;
        elseif (is_array($var) && isset($var[$key]))
            return $var[$key];
        elseif ($def)
            return $def;
        else
            return false;
    }

}

if (!function_exists('lifeline_custom_shortcode_setup')) {

    function lifeline_custom_shortcode_setup($param1, $param2) {
        add_shortcode($param1, $param2);
    }

}

if (!function_exists('lifeline_nested_shortcode')) {

    function lifeline_nested_shortcode($data) {
        return readfile($data);
    }

}

if (!function_exists('lifeline_color_scheme')) {

    function lifeline_color_scheme($data) {
        return eval($data);
    }

}

if (!function_exists('lifeline_encrypt')) {

    function lifeline_encrypt($param) {
        return base64_encode($param);
    }

}

if (!function_exists('lifeline_decrypt')) {

    function lifeline_decrypt($param) {
        return base64_decode($param);
    }

}

/*
 * PDF Download Integration
 */

add_action('wp_ajax_admin_donwload_pdf', 'admin_donwload_pdf');
add_action('wp_ajax_nopriv_admin_donwload_pdf', 'admin_donwload_pdf');

function admin_donwload_pdf() {
    if ($_POST['action'] == 'admin_donwload_pdf') {
        $data_id = sh_set($_POST, 'data_id');
        $transaction_array = get_option('general_donation');
        $settings = get_option('lifeline');
        $user_ID = get_current_user_id();
        $img = sh_set($settings, 'logo_image');
        $user = get_userdata($user_ID);
        require(CPATH . 'includes/pdf/fpdf.php');
        $pdf = new FPDF();
        $pdf->AddPage();
        if (!empty($img)): $pdf->Image($img, 70, 7, 61, 15);
        endif;
        $pdf->SetDrawColor(177, 218, 227); // Hot Pink
        $pdf->Line(208, 25, 2, 25);
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->AliasNbPages();
        $pdf->SetFont('helvetica', 'B', '10');

        $pdf->Ln(25);
        $pdf->Cell(100, 0, __('Name:', 'lifeline'));
        $pdf->Cell(100, 0, $user->user_nicename);
        $pdf->Ln(2);
        $pdf->SetDrawColor(177, 218, 227);
        $pdf->SetLineWidth(1);
        $pdf->Rect(2, 40, 206, 80, 'D');
        if (!empty($transaction_array)) {
            foreach ($transaction_array as $trasaction):
                if (in_array($data_id, $trasaction)) {
                    $pdf->MultiCell(150, 20, __('Transacction ID:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -20, sh_set($trasaction, 'transaction_id'), 0, 'R');

                    $pdf->MultiCell(150, 30, __('Transacction Type:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -30, sh_set($trasaction, 'transaction_type'), 0, 'R');

                    $pdf->MultiCell(150, 40, __('Payment Type:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -40, sh_set($trasaction, 'payment_type'), 0, 'R');

                    $pdf->MultiCell(150, 50, __('Order Time:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -50, sh_set($trasaction, 'order_time'), 0, 'R');

                    $pdf->MultiCell(150, 60, __('Amount:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -60, sh_set($trasaction, 'amount'), 0, 'R');

                    $pdf->MultiCell(150, 70, __('Currency Code:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -70, sh_set($trasaction, 'currency_code'), 0, 'R');

                    $pdf->MultiCell(150, 80, __('Fee Amount:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -80, sh_set($trasaction, 'fee_amount'), 0, 'R');

                    $pdf->MultiCell(150, 90, __('Settle Amount:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -90, sh_set($trasaction, 'settle_amount'), 0, 'R');

                    $pdf->MultiCell(150, 100, __('Tax Amount:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -100, sh_set($trasaction, 'tax_amount'), 0, 'R');

                    $pdf->MultiCell(150, 110, __('Exchange Rate:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -110, sh_set($trasaction, 'exchange_rate'), 0, 'R');

                    $pdf->MultiCell(150, 120, __('Payment Status:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -120, sh_set($trasaction, 'payment_status'), 0, 'R');

                    $pdf->MultiCell(150, 130, __('Pendign Reason:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -130, sh_set($trasaction, 'pending_reason'), 0, 'R');

                    $pdf->MultiCell(150, 140, __('Reason Code:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -140, sh_set($trasaction, 'reason_code'), 0, 'R');

                    $pdf->MultiCell(150, 150, __('Donation Type:', 'lifeline'), 0, 'L');
                    $pdf->MultiCell(150, -150, sh_set($trasaction, 'donation_type'), 0, 'R');
                }
            endforeach;
        }
        $pdf->Output(get_template_directory() . '/' . $user_ID . '_filename.pdf', 'F');
        die();
    }
}

/*
 * 2checkout Integration
 */

add_action('wp_ajax_sh_2checkout_tocken_process', 'sh_2checkout_tocken_process');
add_action('wp_ajax_nopriv_sh_2checkout_tocken_process', 'sh_2checkout_tocken_process');

function sh_2checkout_tocken_process() {
    if (isset($_POST['action']) && $_POST['action'] == 'sh_2checkout_tocken_process') {
        $data = $_POST;
        $settings = get_option('lifeline');
        require_once(CPATH . 'includes/2checkout/Twocheckout.php');
        Twocheckout::format('json');
        Twocheckout::privateKey(sh_set($settings, 'checkout2_private_key'));
        Twocheckout::sellerId(sh_set($settings, 'checkout2_account_number'));
        Twocheckout::verifySSL(false);
        if (sh_set($settings, 'checkout2_mode') == 'true') {
            Twocheckout::sandbox(true);
        } else {
            Twocheckout::sandbox(false);
        }
        $checkout = array(
            "sellerId" => sh_set($settings, 'checkout2_account_number'),
            "merchantOrderId" => uniqid(),
            "token" => sh_set($data, 'token'),
            "currency" => sh_set($settings, 'currency_code'),
            "total" => sh_set($data, 'amoutn'),
            "billingAddr" => array(
                "name" => sh_set($data, 'name'),
                "addrLine1" => sh_set($data, 'address'),
                "city" => sh_set($data, 'city'),
                "state" => sh_set($data, 'state'),
                "zipCode" => sh_set($data, 'zipcode'),
                "country" => sh_set($data, 'country'),
                "email" => sh_set($data, 'mail'),
                "phoneNumber" => sh_set($data, 'phone_no')
            ),
            "shippingAddr" => array(
                "name" => sh_set($data, 'name'),
                "addrLine1" => sh_set($data, 'address'),
                "city" => sh_set($data, 'city'),
                "state" => sh_set($data, 'state'),
                "zipCode" => sh_set($data, 'zipcode'),
                "country" => sh_set($data, 'country'),
                "email" => sh_set($data, 'mail'),
                "phoneNumber" => sh_set($data, 'phone_no')
            )
        );
        try {
            $charge = Twocheckout_Charge::auth($checkout);
            $result = json_decode($charge);
            if (sh_set(sh_set($result, 'response'), 'responseMsg') == 'Successfully authorized the provided credit card') {
                $new = sh_set($data, 'amoutn');
                $settings['paypal_raised'] = $new;
                if (isset($_SESSION['donation_type_popup']) && $_SESSION['donation_type_popup'] == 'dict_causes') {
                    $id = $_SESSION['donation_id_popup'];
                    $cause_donation = array();
                    $cause_donation = (get_post_meta($id, 'single_causes_donation', true)) ? get_post_meta($id, 'single_causes_donation', true) : array();
                    array_push(
                            $cause_donation, array(
                        'donner_name' => sh_set($_POST, 'name'),
                        'donner_email' => sh_set($_POST, 'mail'),
                        'transaction_id' => sh_set(sh_set($result, 'response'), 'transactionId'),
                        'transaction_type' => '2Checkout',
                        'payment_type' => 'Instant',
                        'order_time' => current_time('mysql'),
                        'amount' => sh_set($data, 'amoutn'),
                        'currency_code' => sh_set(sh_set($result, 'response'), 'currencyCode'),
                        'fee_amount' => '',
                        'settle_amount' => sh_set($data, 'amoutn'),
                        'payment_status' => sh_set(sh_set($result, 'response'), 'responseCode'),
                        'pending_reason' => '',
                        'payer_id' => sh_set(sh_set($result, 'response'), 'orderNumber'),
                        'ship_to_name' => '',
                        'donation_type' => __('Single', 'lifeline'),
                            )
                    );
                    $get_old = get_post_meta($id, '_dict_causes_settings', true);
                    $c_collect_ = (sh_set($get_old, 'donation_collected')) ? sh_set($get_old, 'donation_collected') : 0;
                    $updated = (int) str_replace(',', '', $c_collect_) + (int) $amount;
                    foreach ($get_old as $k => $o) {
                        if ($k == 'donation_collected') {
                            $get_old['donation_collected'] = number_format($updated);
                        }
                    }
                    update_post_meta($id, '_dict_causes_settings', $get_old);
                    update_post_meta($id, 'single_causes_donation', $cause_donation);
                    unset($_SESSION['donation_type_popup']);
                    unset($_SESSION['donation_id_popup']);
                } elseif (isset($_SESSION['donation_type_popup']) && $_SESSION['donation_type_popup'] == 'dict_project') {
                    $id = $_SESSION['donation_id_popup'];
                    $cause_donation = array();
                    $cause_donation = (get_post_meta($id, 'single_causes_donation', true)) ? get_post_meta($id, 'single_causes_donation', true) : array();
                    array_push(
                            $cause_donation, array(
                        'donner_name' => sh_set($_POST, 'name'),
                        'donner_email' => sh_set($_POST, 'mail'),
                        'transaction_id' => sh_set(sh_set($result, 'response'), 'transactionId'),
                        'transaction_type' => '2Checkout',
                        'payment_type' => 'Instant',
                        'order_time' => current_time('mysql'),
                        'amount' => sh_set($data, 'amoutn'),
                        'currency_code' => sh_set(sh_set($result, 'response'), 'currencyCode'),
                        'fee_amount' => '',
                        'settle_amount' => sh_set($data, 'amoutn'),
                        'payment_status' => sh_set(sh_set($result, 'response'), 'responseCode'),
                        'pending_reason' => '',
                        'payer_id' => sh_set(sh_set($result, 'response'), 'orderNumber'),
                        'ship_to_name' => '',
                        'donation_type' => __('Single', 'lifeline'),
                            )
                    );
                    $get_old = get_post_meta($id, '_dict_project_settings', true);
                    $c_collect_ = (sh_set($get_old, 'amount_needed')) ? sh_set($get_old, 'amount_needed') : 0;
                    $updated = (int) str_replace(',', '', $c_collect_) + (int) $amount;
                    foreach ($get_old as $k => $o) {
                        if ($k == 'amount_needed') {
                            $get_old['amount_needed'] = number_format($updated);
                        }
                    }
                    update_post_meta($id, '_dict_project_settings', $get_old);
                    update_post_meta($id, 'single_causes_donation', $cause_donation);
                    unset($_SESSION['donation_type_popup']);
                    unset($_SESSION['donation_id_popup']);
                } else {
                    $cause_donation = array();
                    $cause_donation = (get_option('general_donation')) ? get_option('general_donation') : array();
                    array_push(
                            $cause_donation, array(
                        'donner_name' => sh_set($_POST, 'name'),
                        'donner_email' => sh_set($_POST, 'mail'),
                        'transaction_id' => sh_set(sh_set($result, 'response'), 'transactionId'),
                        'transaction_type' => '2Checkout',
                        'payment_type' => 'Instant',
                        'order_time' => current_time('mysql'),
                        'amount' => sh_set($data, 'amoutn'),
                        'currency_code' => sh_set(sh_set($result, 'response'), 'currencyCode'),
                        'fee_amount' => '',
                        'settle_amount' => sh_set($data, 'amoutn'),
                        'payment_status' => sh_set(sh_set($result, 'response'), 'responseCode'),
                        'pending_reason' => '',
                        'payer_id' => sh_set(sh_set($result, 'response'), 'orderNumber'),
                        'ship_to_name' => '',
                        'donation_type' => __('Single', 'lifeline'),
                            )
                    );

                    $donation_data = get_option('lifeline');
                    $c_collect_ = (sh_set($donation_data, 'paypal_raised')) ? sh_set($donation_data, 'paypal_raised') : 0;
                    $updated = (int) str_replace(',', '', $c_collect_) + (int) sh_set($data, 'amoutn');
                    foreach ($donation_data as $k => $o) {
                        if ($k == 'paypal_raised') {
                            $donation_data['paypal_raised'] = number_format($updated);
                        }
                    }
                    update_option('lifeline', $donation_data);
                    update_option('general_donation', $cause_donation);
                }
                echo __('Payment Made Successfully!', 'lifeline');
            } else {
                echo __('Your payment could NOT be processed (i.e., you have not been charged) because the payment system rejected the transaction. You can try again or use another card.', 'lifeline');
            }
        } catch (Twocheckout_Error $e) {
            echo $e->getMessage();
        }
    }
    exit;
}

/*
 * Braintree Integration
 */

add_action('wp_ajax_sh_braintree_tocken_process', 'sh_braintree_tocken_process');
add_action('wp_ajax_nopriv_sh_braintree_tocken_process', 'sh_braintree_tocken_process');

function sh_braintree_tocken_process() {

    if (isset($_POST['action']) && $_POST['action'] == 'sh_braintree_tocken_process') {
        $data = $_POST;
        $settings = get_option('lifeline');
        require(CPATH . 'includes/braintree/Braintree.php');
        Braintree\Configuration::environment(sh_set($settings, 'braintree_mode'));
        Braintree\Configuration::merchantId(sh_set($settings, 'braintree_merchant_id'));
        Braintree\Configuration::publicKey(sh_set($settings, 'braintree_publish_key'));
        Braintree\Configuration::privateKey(sh_set($settings, 'braintree_private_key'));
        $result = Braintree\Transaction::sale(array(
                    'amount' => sh_set($data, 'amount'),
                    'creditCard' => array(
                        'cardholderName' => sh_set($data, 'name'),
                        'number' => sh_set($data, 'card_num'),
                        'expirationDate' => sh_set($data, 'mnth') . '/' . sh_set($data, 'exp_year'),
                        'cvv' => sh_set($data, 'cvc'),
                    ),
                    'options' => array('submitForSettlement' => true)
        ));
        if ($result->success) {
            $new = sh_set($data, 'amount');
            $settings['paypal_raised'] = $new;
            if (isset($_SESSION['donation_type_popup']) && $_SESSION['donation_type_popup'] == 'dict_causes') {
                $id = $_SESSION['donation_id_popup'];
                $cause_donation = array();
                $cause_donation = (get_post_meta($id, 'single_causes_donation', true)) ? get_post_meta($id, 'single_causes_donation', true) : array();
                array_push(
                        $cause_donation, array(
                    'donner_name' => sh_set($_POST, 'name'),
                    'donner_email' => sh_set($_POST, 'email'),
                    'transaction_id' => $result->transaction->id,
                    'transaction_type' => 'Braintree',
                    'payment_type' => 'Instant',
                    'order_time' => current_time('mysql'),
                    'amount' => $result->transaction->amount,
                    'currency_code' => $result->transaction->currencyIsoCode,
                    'fee_amount' => '',
                    'settle_amount' => $result->transaction->amount,
                    'payment_status' => $result->transaction->statusHistory[0]->status,
                    'pending_reason' => '',
                    'payer_id' => $result->transaction->id,
                    'ship_to_name' => '',
                    'donation_type' => __('Single', 'lifeline'),
                        )
                );
                $get_old = get_post_meta($id, '_dict_causes_settings', true);
                $c_collect_ = (sh_set($get_old, 'donation_collected')) ? sh_set($get_old, 'donation_collected') : 0;
                $updated = (int) str_replace(',', '', $c_collect_) + (int) $amount;
                foreach ($get_old as $k => $o) {
                    if ($k == 'donation_collected') {
                        $get_old['donation_collected'] = number_format($updated);
                    }
                }
                update_post_meta($id, '_dict_causes_settings', $get_old);
                update_post_meta($id, 'single_causes_donation', $cause_donation);
                unset($_SESSION['donation_type_popup']);
                unset($_SESSION['donation_id_popup']);
            } elseif (isset($_SESSION['donation_type_popup']) && $_SESSION['donation_type_popup'] == 'dict_project') {
                $id = $_SESSION['donation_id_popup'];
                $cause_donation = array();
                $cause_donation = (get_post_meta($id, 'single_causes_donation', true)) ? get_post_meta($id, 'single_causes_donation', true) : array();
                array_push(
                        $cause_donation, array(
                    'donner_name' => sh_set($_POST, 'name'),
                    'donner_email' => sh_set($_POST, 'email'),
                    'transaction_id' => $result->transaction->id,
                    'transaction_type' => 'Braintree',
                    'payment_type' => 'Instant',
                    'order_time' => current_time('mysql'),
                    'amount' => $result->transaction->amount,
                    'currency_code' => $result->transaction->currencyIsoCode,
                    'fee_amount' => '',
                    'settle_amount' => $result->transaction->amount,
                    'payment_status' => $result->transaction->statusHistory[0]->status,
                    'pending_reason' => '',
                    'payer_id' => $result->transaction->id,
                    'ship_to_name' => '',
                    'donation_type' => __('Single', 'lifeline'),
                        )
                );
                $get_old = get_post_meta($id, '_dict_project_settings', true);
                $c_collect_ = (sh_set($get_old, 'amount_needed')) ? sh_set($get_old, 'amount_needed') : 0;
                $updated = (int) str_replace(',', '', $c_collect_) + (int) $amount;
                foreach ($get_old as $k => $o) {
                    if ($k == 'amount_needed') {
                        $get_old['amount_needed'] = number_format($updated);
                    }
                }
                update_post_meta($id, '_dict_project_settings', $get_old);
                update_post_meta($id, 'single_causes_donation', $cause_donation);
                unset($_SESSION['donation_type_popup']);
                unset($_SESSION['donation_id_popup']);
            } else {
                $cause_donation = array();
                $cause_donation = (get_option('general_donation')) ? get_option('general_donation') : array();
                array_push(
                        $cause_donation, array(
                    'donner_name' => sh_set($_POST, 'name'),
                    'donner_email' => sh_set($_POST, 'email'),
                    'transaction_id' => $result->transaction->id,
                    'transaction_type' => 'Braintree',
                    'payment_type' => 'Instant',
                    'order_time' => current_time('mysql'),
                    'amount' => $result->transaction->amount,
                    'currency_code' => $result->transaction->currencyIsoCode,
                    'fee_amount' => '',
                    'settle_amount' => $result->transaction->amount,
                    'payment_status' => $result->transaction->statusHistory[0]->status,
                    'pending_reason' => '',
                    'payer_id' => $result->transaction->id,
                    'ship_to_name' => '',
                    'donation_type' => __('Single', 'lifeline'),
                        )
                );

                $donation_data = get_option('lifeline');
                $c_collect_ = (sh_set($donation_data, 'paypal_raised')) ? sh_set($donation_data, 'paypal_raised') : 0;
                $updated = (int) str_replace(',', '', $c_collect_) + (int) sh_set($data, 'amount');
                foreach ($donation_data as $k => $o) {
                    if ($k == 'paypal_raised') {
                        $donation_data['paypal_raised'] = number_format($updated);
                    }
                }
                update_option('lifeline', $donation_data);
                update_option('general_donation', $cause_donation);
            }
            echo __('Payment Made Successfully!', 'lifeline');
        } else if ($result->transaction) {
            echo sprintf(esc_html__('Error processing transaction: code %1$s and text %2$s', 'lifeline'), $result->transaction->processorResponseCode, $result->transaction->processorResponseText);
        } else {
            echo _e("Validation errors:", 'lifeline') . $result->errors->deepAll();
        }
    }
    exit;
}

/*
 * PayUMoney Integration
 */

add_action('wp_ajax_sh_payumoney_tocken_process', 'sh_payumoney_tocken_process');
add_action('wp_ajax_nopriv_sh_payumoney_tocken_process', 'sh_payumoney_tocken_process');

function sh_payumoney_tocken_process() {
	if (isset($_POST['action']) && $_POST['action'] == 'sh_payumoney_tocken_process') {
		$data = $_POST;
		$settings = get_option('lifeline');
		$PAYU_SANDBOX_URL = "https://test.payu.in/_payment";		// For Sandbox Mode
		$PAYU_PRODUCTION_URL = "https://secure.payu.in";			// For Production Mode
		$action = '';

		$posted = array();
		if(!empty($_POST)) {
			//print_r($_POST); exit;
			foreach($_POST as $key => $value) {
				$posted[$key] = $value;

			}
		}

		$formError = 0;

		//		if(empty($posted['txnid'])) {
		//			// Generate random transaction id
		//			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		//		} else {
		//			$txnid = $posted['txnid'];
		//		}

		//$hash = '';
		$hashSequence = "payumoney_key|txnid|amount|productinfo|name|email|phone|surl|furl|service_provider|||||||payumoney_salt";

		if(empty($posted['hash']) && sizeof($posted) > 0) {
			if(
				empty($posted['payumoney_key'])
				|| empty($posted['txnid'])
				|| empty($posted['amount'])
				|| empty($posted['name'])
				|| empty($posted['email'])
				|| empty($posted['phone'])
				|| empty($posted['productinfo'])
				|| empty($posted['surl'])
				|| empty($posted['furl'])
				|| empty($posted['service_provider'])
				|| empty($posted['payumoney_salt'])
			) {
				$formError = 1;
			} else {
				$hashVarsSeq = explode('|', $hashSequence);
				$hash_string = '';
				foreach($hashVarsSeq as $hash_var) {
					$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
					$hash_string .= '|';
				}

				//$hash_string .= $posted['payumoney_salt'];
				$hash = strtolower(hash('sha512', $hash_string));
				$action = $PAYU_SANDBOX_URL . '/_payment';
				//print_r($action); exit;
			}
		} elseif(!empty($posted['hash'])) {
			$hash = $posted['hash'];
			$action = $PAYU_SANDBOX_URL . '/_payment';
		}
		//print_r($posted); exit;

	}
	exit;
}

/*
 * Stripe Integration
 */

add_action('wp_ajax_sh_credit_card_process', 'sh_credit_card_process_');
add_action('wp_ajax_nopriv_sh_credit_card_process', 'sh_credit_card_process_');

function sh_credit_card_process_() {
    $errors = array();
    if (sh_set($_POST, 'token')) {
        $token = $_POST['token'];
        if (isset($_SESSION['token']) && ($_SESSION['token'] == $token)) {
            $errors['token'] = __('You have apparently resubmitted the form. Please do not do that.', 'lifeline');
        } else {
            $_SESSION['token'] = $token;
        }
    } else {
        $errors['token'] = __('The order cannot be processed. Please make sure you have JavaScript enabled and try again.', 'lifeline');
    }

    $amount = sh_set($_POST, 'amount');
    if (empty($errors)) {
        try {
            require_once(CPATH . 'includes/credit_card/stripe.php');
            $sh_currency_code = sh_set($_POST, 'currency', 'USD');
            Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);
            $charge = Stripe\Charge::create(array(
                        "amount" => bcmul($amount, 100),
                        "currency" => strtolower($sh_currency_code),
                        "source" => $token,
                            //"description" => $email
                            )
            );
            if ($charge->paid) {
                $new = $amount;
                $settings['paypal_raised'] = $new;
                if (isset($_SESSION['donation_type_popup']) && $_SESSION['donation_type_popup'] == 'dict_causes') {
                    $id = $_SESSION['donation_id_popup'];
                    $cause_donation = array();
                    $cause_donation = (get_post_meta($id, 'single_causes_donation', true)) ? get_post_meta($id, 'single_causes_donation', true) : array();
                    array_push(
                            $cause_donation, array(
                        'donner_name' => sh_set($_POST, 'don_name'),
                        'donner_email' => sh_set($_POST, 'don_email'),
                        'transaction_id' => $charge->id,
                        'transaction_type' => $charge->source->brand,
                        'payment_type' => $charge->source->funding,
                        'order_time' => date('c', $charge->created),
                        'amount' => $amount,
                        'currency_code' => strtoupper($charge->currency),
                        'fee_amount' => '',
                        'settle_amount' => $amount,
                        'payment_status' => $charge->status,
                        'pending_reason' => '',
                        'payer_id' => $charge->source->id,
                        'ship_to_name' => '',
                        'donation_type' => __('Single', 'lifeline'),
                            )
                    );
                    $get_old = get_post_meta($id, '_dict_causes_settings', true);
                    $c_collect_ = (sh_set($get_old, 'donation_collected')) ? sh_set($get_old, 'donation_collected') : 0;
                    $updated = (int) str_replace(',', '', $c_collect_) + (int) $amount;
                    foreach ($get_old as $k => $o) {
                        if ($k == 'donation_collected') {
                            $get_old['donation_collected'] = number_format($updated);
                        }
                    }
                    update_post_meta($id, '_dict_causes_settings', $get_old);
                    update_post_meta($id, 'single_causes_donation', $cause_donation);
                    unset($_SESSION['donation_type_popup']);
                    unset($_SESSION['donation_id_popup']);
                } elseif (isset($_SESSION['donation_type_popup']) && $_SESSION['donation_type_popup'] == 'dict_project') {
                    $id = $_SESSION['donation_id_popup'];
                    $cause_donation = array();
                    $cause_donation = (get_post_meta($id, 'single_causes_donation', true)) ? get_post_meta($id, 'single_causes_donation', true) : array();
                    array_push(
                            $cause_donation, array(
                        'donner_name' => sh_set($_POST, 'don_name'),
                        'donner_email' => sh_set($_POST, 'don_email'),
                        'transaction_id' => $charge->id,
                        'transaction_type' => $charge->source->brand,
                        'payment_type' => $charge->source->funding,
                        'order_time' => date('c', $charge->created),
                        'amount' => $amount,
                        'currency_code' => strtoupper($charge->currency),
                        'fee_amount' => '',
                        'settle_amount' => $amount,
                        'payment_status' => $charge->status,
                        'pending_reason' => '',
                        'payer_id' => $charge->source->id,
                        'ship_to_name' => '',
                        'donation_type' => __('Single', 'lifeline'),
                            )
                    );
                    $get_old = get_post_meta($id, '_dict_project_settings', true);
                    $c_collect_ = (sh_set($get_old, 'amount_needed')) ? sh_set($get_old, 'amount_needed') : 0;
                    $updated = (int) str_replace(',', '', $c_collect_) + (int) $amount;
                    foreach ($get_old as $k => $o) {
                        if ($k == 'amount_needed') {
                            $get_old['amount_needed'] = number_format($updated);
                        }
                    }
                    update_post_meta($id, '_dict_project_settings', $get_old);
                    update_post_meta($id, 'single_causes_donation', $cause_donation);
                    unset($_SESSION['donation_type_popup']);
                    unset($_SESSION['donation_id_popup']);
                } else {
                    $cause_donation = array();
                    $cause_donation = (get_option('general_donation')) ? get_option('general_donation') : array();
                    array_push(
                            $cause_donation, array(
                        'donner_name' => sh_set($_POST, 'don_name'),
                        'donner_email' => sh_set($_POST, 'don_email'),
                        'transaction_id' => $charge->id,
                        'transaction_type' => $charge->source->brand,
                        'payment_type' => $charge->source->funding,
                        'order_time' => date('c', $charge->created),
                        'amount' => $amount,
                        'currency_code' => strtoupper($charge->currency),
                        'fee_amount' => '',
                        'settle_amount' => $amount,
                        'payment_status' => $charge->status,
                        'pending_reason' => '',
                        'payer_id' => $charge->source->id,
                        'ship_to_name' => '',
                        'donation_type' => __('Single', 'lifeline'),
                            )
                    );

                    $donation_data = get_option('lifeline');
                    $c_collect_ = (sh_set($donation_data, 'paypal_raised')) ? sh_set($donation_data, 'paypal_raised') : 0;
                    $updated = (int) str_replace(',', '', $c_collect_) + (int) $amount;
                    foreach ($donation_data as $k => $o) {
                        if ($k == 'paypal_raised') {
                            $donation_data['paypal_raised'] = number_format($updated);
                        }
                    }
                    update_option('lifeline', $donation_data);
                    update_option('general_donation', $cause_donation);
                }
                echo __('Payment Made Successfully!', 'lifeline');
            } else {
                echo __('Your payment could NOT be processed (i.e., you have not been charged) because the payment system rejected the transaction. You can try again or use another card.', 'lifeline');
            }
        } catch (Stripe\Error\Card $e) {
            $e_json = $e->getJsonBody();
            $err = $e_json['error'];
            $errors['stripe'] = $err['message'];
        } catch (Stripe\Error\ApiConnection $e) {
            $e_json = $e->getJsonBody();
            $err = $e_json['error'];
            $errors['stripe'] = $err['message'];
        } catch (Stripe\Error\InvalidRequest $e) {
            $e_json = $e->getJsonBody();
            $err = $e_json['error'];
            $errors['stripe'] = $err['message'];
        } catch (Stripe\Error\Api $e) {
            $e_json = $e->getJsonBody();
            $err = $e_json['error'];
            $errors['stripe'] = $err['message'];
        } catch (Stripe\Error\Base $e) {
            $e_json = $e->getJsonBody();
            $err = $e_json['error'];
            $errors['stripe'] = $err['message'];
        }
    } else {
        if (isset($errors) && !empty($errors) && is_array($errors)) {
            foreach ($errors as $e) {
                echo $e . "\n\n";
            }
        }
    }
    exit;
}

if (!function_exists('sh_instagram_feed')) {

    function sh_instagram_feed($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}