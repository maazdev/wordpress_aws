<?php
/*
  Plugin Name: Lifeline
  Plugin URI: http://www.webinane.com
  Description: A utitlity plugin for Lifeline Wordpress Theme
  Author: webinane
  Author URI: http://www.webinane.com
  Version: 1.8.3
  Text Domain: wp_lifeline
  License: GPLv2
  License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */
define('PLUGIN_URI', plugins_url('lifeline') . '/');
define('CPATH', plugin_dir_path(__FILE__));
define('CURL', plugin_dir_url(__FILE__));

load_plugin_textdomain( 'wp_lifeline', false, CPATH.'/languages'); 

/*
 * Includes classes
 */
//require_once(plugin_dir_path(__FILE__) . '/classes/post_types.php');
require_once(plugin_dir_path(__FILE__) . '/classes/taxonomies.php');
require_once(plugin_dir_path(__FILE__) . '/classes/newsletter.php');
require_once(plugin_dir_path(__FILE__) . '/classes/instagram.php');

/*
 * Includes third party libraries
 */
require_once('includes/functions.php');
require_once('includes/codebird.php');
require_once('includes/recaptchalib.php');
if(!class_exists('SH_Grab')) {
	require_once( 'includes/grabber/grab.php' );
}
add_action('plugins_loaded', array('SH_Newsletter', 'lifeline_web_newsletter_table'));
