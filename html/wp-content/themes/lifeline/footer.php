</div>
<?php
$settings = get_option( 'lifeline' );
$footerbg_color=sh_set($settings, 'footer_color_scheme' );
if ( sh_set( $settings, 'show_footer' ) == 'true' ):

	$FooterStyle = ' style="';
	$FooterStyle .= ( isset( $settings['footer_font_family'] ) && !empty( $settings['footer_font_family'] ) ) ? 'font-family:' . $settings['footer_font_family'] . ';' : '';
	$FooterStyle .= ( sh_set( $settings, 'footer_bg' ) ) ? 'background-image:url(' . sh_set( $settings, 'footer_bg' ) . ');' : '';
        $FooterStyle .= ( $footerbg_color ) ? 'background:'.$footerbg_color.' !important;' : '';
	$FooterStyle .= '"';

	?>

	<footer <?php echo (sh_set($settings, 'footer_light') == 'true') ? ' class="light-footer" ' : '';?><?php echo $FooterStyle; ?>>
		<div class="container">
			<div class="row">
				<?php dynamic_sidebar( 'footer-sidebar' ); ?>
			</div>
		</div>
	</footer>

	<?php
endif;
?>



<div class="footer-bottom <?php if(sh_set( $settings, 'sticky_footer' ) == 'true'){ echo ('sticky-footer-bottom'); } ?>">

    <div class="container">
        <?php if(sh_set( $settings, 'footer_copyright' )):?>
            <p><span> <?php echo stripslashes( sh_set( $settings, 'footer_copyright' ) ); ?></span></p>
        <?php endif;
        if (has_nav_menu('footer_menu')):
         wp_nav_menu( array( 'theme_location' => 'footer_menu' ) ); 
        endif;?>

    </div>

</div>

<?php wp_footer(); ?>
<script type="text/javascript">Stripe.setPublishableKey('<?php echo STRIPE_PUBLIC_KEY; ?>');</script>
<script src="https://www.paypalobjects.com/api/checkout.js" async></script>
 <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
 <script>
$.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });   
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           }); 
		   
		   </script>
</body>

</html>
