<?php

//include(get_template_directory().'/includes/resource/awesom_icons.php');

$settings = array();

$settings['page'] = array(
    array('section_name' => 'Header Settings',
        'section_id' => 'header',
        'position' => 'normal',
        'fields' => array(
            'is_home' =>
            array(
                'type' => 'checkbox', //builtin fields include:
                'id' => 'is_home',
                'title' => __('Make Homepage', 'lifeline'),
                'desc' => '',
                'help' => __("Make This is a Homepage", 'lifeline'),
            ),
            'header' =>
            array(
                'type' => 'radio', //builtin fields include:
                'id' => 'header',
                'title' => __('Show Header', 'lifeline'),
                'options' => array('true' => 'True', 'false' => 'False'),
                'attributes' => array('style' => 'width:40%'),
                'help' => __("Make it false If This is a Homepage", 'lifeline'),
            ),
            'top_image' =>
            array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'attributes' => array('placeholder' => __('Upload Page Top Image', 'lifeline')),
            ),
            'post_type' =>
            array(
                'type' => 'select', //builtin fields include:
                'id' => 'tpl_post_type',
                'title' => __('Select Category', 'lifeline'),
                'options' => sh_get_categories(array('hide_empty' => FALSE)),
                'help' => __("Note: This Option is only work for 'Our Mission' and 'Recent News' Template, Otherwise leave it", 'lifeline'),
            ),
            'show_page_title' =>
            array(
                'type' => 'checkbox', //builtin fields include:
                'id' => 'show_page_title',
                'title' => __('Show Page Title', 'lifeline'),
                'desc' => '',
                'help' => __("Show page title", 'lifeline'),
            ),
            'page_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'page_title',
                'title' => __('Page Title', 'lifeline'),
                'help' => __("Enter Page Title(if you want to show page title) .", 'lifeline'),
            ),
            'content_limit' =>
	            array(
		            'type' => 'text', //builtin fields include:
		            'id' => 'content_limit',
		            'title' => __('Content Limit', 'lifeline'),
		            'help' => __("Enter the character limit for content.", 'lifeline'),
	            ),
        )
    ),
    array('section_name' => 'Sidebar Settings',
        'section_id' => 'sidebar',
        'position' => 'side',
        'fields' => array(
            'sidebar' =>
            array(
                'type' => 'select', //builtin fields include:
                'id' => 'sidebar',
                'title' => __('Sidebar', 'lifeline'),
                'options' => sh_get_sidebars(),
                'help' => __("This Option is Not Supported in Default Page Template", 'lifeline'),
            ),
            'sidebar_pos' => array(
                'type' => 'radio', //builtin fields include:
                'id' => 'sidebar_pos',
                'title' => __('Sidebar Postion', 'lifeline'),
                'options' => array('left' => 'Left', 'right' => 'Right'),
                'attributes' => array('style' => 'width:40%'),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);

$settings['product'] = array(
    array('section_name' => 'Header Settings',
        'section_id' => 'header',
        'position' => 'normal',
        'fields' => array(
            'top_image' =>
            array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'help' => __("This is short help", 'lifeline'),
                'attributes' => array('placeholder' => __('Upload Page Top Image', 'lifeline')),
            ),
        )
    ),
);


$settings['dict_testimonials'] = array(
    array('section_name' => 'Generel Settings',
        'section_id' => 'general_info',
        'position' => 'normal',
        'fields' => array(
            'name' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'name',
                'title' => __('Name', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter name', 'lifeline')),
            ),
            'designation' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'designation',
                'title' => __('Designation', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter designation', 'lifeline')),
            ),
            'location' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'location',
                'title' => __('Location', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter location', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
$settings['dict_causes'] = array(
    array('section_name' => 'Donation Settings',
        'section_id' => 'donation',
        'position' => 'normal',
        'fields' => array(
            'start_date' => array(
                'type' => 'date', //builtin fields include:
                'id' => 'start_date',
                'title' => __('Start Date', 'lifeline'),
                'desc' => __('Enter event start date', 'lifeline'),
                'attributes' => array('placeholder' => 'YYYY-MM-DD', 'style' => 'width:40%'),
            ),
            'end_date' => array(
                'type' => 'date', //builtin fields include:
                'id' => 'end_date',
                'title' => __('End Date', 'lifeline'),
                'desc' => __('Enter event end date', 'lifeline'),
                'attributes' => array('placeholder' => 'YYYY-MM-DD', 'style' => 'width:40%'),
            ),
            'show_donation_bar' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'show_donation_bar',
                'title' => __('Show Donation Bar', 'lifeline'),
                'options' => array(
                    'false' => __('Hide', 'lifeline'),
                    'true' => __('Show', 'lifeline'),
                ),
            ),
            'currency_symbol' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'currency_symbol',
                'title' => __('Donation Neede Currency Symbol', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter needed donation currency symbol', 'lifeline')),
            ),
            'currency_code' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'currency_code',
                'title' => __('Currency Code', 'lifeline'),
                'options' => sh_get_currencies()
            ),
            'donation_needed' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'donation_needed',
                'title' => __('Donation Needed', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter needed donation', 'lifeline')),
            ),
            'donation_collected' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'donation_collected',
                'title' => __('Donation Collected', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter collected donation', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'Video Link',
        'section_id' => 'video_link',
        'position' => 'normal',
        'fields' => array(
            'video_link' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'video_link',
                'title' => __('Video Link', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter video Link', 'lifeline')),
                'desc' => __('Enter the the video link for this cause', 'lifeline'),
            ),
        )
    ),
    array('section_name' => 'Donation Transactions',
        'section_id' => 'donation_transactions',
        'position' => 'normal',
        'fields' => array(
            'transactions' =>
            array(
                'type' => 'transactions', //builtin fields include:
                'id' => 'transactions',
                'title' => __('Donations Trasactions', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter donation location', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'Location',
        'section_id' => 'location',
        'position' => 'normal',
        'fields' => array(
            'location' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'location',
                'title' => __('Location', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter donation location', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'Gallery Settings',
        'section_id' => 'causes_gallery_settings',
        'position' => 'normal',
        'fields' => array(
            'gallery' => array(
                'type' => 'gallery', //builtin fields include:
                'id' => 'gallery',
                'title' => __('Gallery', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
        )
    ),
    array('section_name' => 'Display Settings',
        'section_id' => 'project_setting',
        'position' => 'normal',
        'fields' => array(
            'sidebar_pos' => array(
                'type' => 'radio', //builtin fields include:
                'id' => 'sidebar_pos',
                'title' => __('Sidebar Postion', 'lifeline'),
                'options' => array('left' => 'Left', 'right' => 'Right'),
                'attributes' => array('style' => 'width:40%'),
            ),
            'sidebar' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'sidebar',
                'title' => __('Sidebar', 'lifeline'),
                'options' => sh_get_sidebars(),
            ),
            'top_image' => array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'attributes' => array('placeholder' => __('Upload project Top Image', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
$settings['dict_project'] = array(
    array('section_name' => 'Project Information',
        'section_id' => 'informaton',
        'position' => 'normal',
        'fields' => array(
            'date' => array(
                'type' => 'date', //builtin fields include:
                'id' => 'date',
                'title' => __('Pick a date', 'lifeline'),
                'std' => '',
                'attributes' => array('placeholder' => __('Pick a Date', 'lifeline')),
            ),
            'location' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'location',
                'title' => __('Location', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter project location', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'Amount Information',
        'section_id' => 'amount_information',
        'position' => 'normal',
        'fields' => array(
            'show_proj_donation_bar' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'show_proj_donation_bar',
                'title' => __('Show Donation Bar', 'lifeline'),
                'options' => array(
                    'true' => __('Show', 'lifeline'),
                    'false' => __('Hide', 'lifeline'),
                ),
            ),
            'spent_amount_currency' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'spent_amount_currency',
                'title' => __('Spent Amount Currency', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter Currency Symbol.', 'lifeline')),
            ),
            'spent_amount' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'spent_amount',
                'title' => __('Spent Amount', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter amount spent on this project.', 'lifeline')),
            ),
            'amount_needed_currency' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'amount_needed_currency',
                'title' => __('Amount Needed Currency', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter Currency Symbol.', 'lifeline')),
            ),
            'amount_needed' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'amount_needed',
                'title' => __('Amount Needed', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter amount needed for this project.', 'lifeline')),
            ),
            'currency_code' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'currency_code',
                'title' => __('Currency Code', 'lifeline'),
                'options' => sh_get_currencies()
            ),
        )
    ),
    array('section_name' => 'Donation Transactions',
        'section_id' => 'donation_transactions',
        'position' => 'normal',
        'fields' => array(
            'transactions' =>
            array(
                'type' => 'transactions', //builtin fields include:
                'id' => 'transactions',
                'title' => __('Donations Trasactions', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter donation location', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'Display Settings',
        'section_id' => 'project_setting',
        'position' => 'normal',
        'fields' => array(
            'sidebar_pos' => array(
                'type' => 'radio', //builtin fields include:
                'id' => 'sidebar_pos',
                'title' => __('Sidebar Postion', 'lifeline'),
                'options' => array('left' => 'Left', 'right' => 'Right'),
                'attributes' => array('style' => 'width:40%'),
            ),
            'sidebar' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'sidebar',
                'title' => __('Sidebar', 'lifeline'),
                'options' => sh_get_sidebars(),
            ),
            'font' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'font',
                'title' => __('Service Icon', 'lifeline'),
                'options' => sh_font_awesome(),
            ),
            'videos' => array(
                'type' => 'multi_text', //builtin fields include:
                'id' => 'videos',
                'title' => __('Videos', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
            'top_image' => array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'attributes' => array('placeholder' => __('Upload project Top Image', 'lifeline')),
            ),
            'margin_bottom' => array(
	            'type' => 'text', //builtin fields include:
	            'id' => 'margin_bottom',
	            'title' => __('Margin Bottom', 'lifeline'),
	            'attributes' => array('placeholder' => __('Give the px value for bottom margin', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
$settings['dict_portfolio'] = array(
    array('section_name' => 'Project Information',
        'section_id' => 'informaton',
        'position' => 'normal',
        'fields' => array(
            'location' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'location',
                'title' => __('Location', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter project location', 'lifeline')),
            ),
            'leader' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'leader',
                'title' => __('Leader', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter project leader', 'lifeline')),
            ),
            'task' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'task',
                'title' => __('Task', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter project Task', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'Display Settings',
        'section_id' => 'project_setting',
        'position' => 'normal',
        'fields' => array(
            'sidebar_pos' => array(
                'type' => 'radio', //builtin fields include:
                'id' => 'sidebar_pos',
                'title' => __('Sidebar Postion', 'lifeline'),
                'options' => array('left' => 'Left', 'right' => 'Right'),
                'attributes' => array('style' => 'width:40%'),
            ),
            'sidebar' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'sidebar',
                'title' => __('Sidebar', 'lifeline'),
                'options' => sh_get_sidebars(),
            ),
            'top_image' => array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'attributes' => array('placeholder' => __('Upload project Top Image', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
$settings['dict_event'] = array(
    array('section_name' => 'Event Information',
        'section_id' => 'event_information',
        'position' => 'normal',
        'fields' => array(
            'start_date' => array(
                'type' => 'date', //builtin fields include:
                'id' => 'start_date',
                'title' => __('Start Date', 'lifeline'),
                'desc' => __('Enter event start date', 'lifeline'),
                'attributes' => array('placeholder' => 'YYYY-MM-DD', 'style' => 'width:40%'),
            ),
            'end_date' => array(
                'type' => 'date', //builtin fields include:
                'id' => 'end_date',
                'title' => __('End Date', 'lifeline'),
                'desc' => __('Enter event end date', 'lifeline'),
                'attributes' => array('placeholder' => 'YYYY-MM-DD', 'style' => 'width:40%'),
            ),
            'start_time' => array(
                'type' => 'timepicker', //builtin fields include:
                'id' => 'start_time',
                'title' => __('Event Start Time', 'lifeline'),
                'desc' => __('Enter event start time', 'lifeline'),
                'attributes' => array('placeholder' => 'HH:MM', 'style' => 'width:40%'),
            ),
            'end_time' => array(
                'type' => 'timepicker', //builtin fields include:
                'id' => 'end_time',
                'title' => __('Event End Time', 'lifeline'),
                'desc' => __('Enter end time start time', 'lifeline'),
                'attributes' => array('placeholder' => 'HH:MM', 'style' => 'width:40%'),
            ),
            'location' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'location',
                'title' => __('Event Location', 'lifeline'),
                'desc' => __('Enter event location', 'lifeline'),
                'attributes' => array(),
            ),
        )
    ),
    array('section_name' => 'Organizer Information',
        'section_id' => 'organizer_information',
        'position' => 'normal',
        'fields' => array(
            'show_organization_section' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'show_organization_section',
                'title' => __('Show Organization Section', 'lifeline'),
                'options' => array(
                    'true' => __('Show', 'lifeline'),
                    'false' => __('Hide', 'lifeline'),
                ),
            ),
            'organizer' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'organizer',
                'title' => __('Organizer', 'lifeline'),
                'desc' => __('Enter the name of the event organizer', 'lifeline'),
                'attributes' => array(),
            ),
            'address' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'address',
                'title' => __('Organizer Address', 'lifeline'),
                'desc' => __('Enter the address of the organizer', 'lifeline'),
                'attributes' => array(),
            ),
            'contact' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'contact',
                'title' => __('Contact Number', 'lifeline'),
                'desc' => __('Enter the contact number', 'lifeline'),
                'attributes' => array('placeholder' => '1 (444) 4564', 'style' => 'width:40%;'),
            ),
            'email' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'email',
                'title' => __('Email ID', 'lifeline'),
                'desc' => __('Enter email ID', 'lifeline'),
                'attributes' => array('placeholder' => 'example@email.com'),
            ),
            'website' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'website',
                'title' => __('Website', 'lifeline'),
                'desc' => __('Enter event website', 'lifeline'),
                'attributes' => array('placeholder' => 'http://www.example.com'),
            ),
        )
    ),
    array('section_name' => 'Display Settings',
        'section_id' => 'display_events',
        'position' => 'normal',
        'fields' => array(
            'sidebar_pos' => array(
                'type' => 'radio', //builtin fields include:
                'id' => 'sidebar_pos',
                'title' => __('Sidebar Postion', 'lifeline'),
                'options' => array('left' => 'Left', 'right' => 'Right'),
                'attributes' => array('style' => 'width:40%'),
            ),
            'sidebar' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'sidebar',
                'title' => __('Sidebar', 'lifeline'),
                'options' => sh_get_sidebars(),
            ),
            'top_image' => array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'attributes' => array('placeholder' => __('Upload Event Top Image', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
$settings['dict_gallery'] = array(
    array('section_name' => 'Gallery Settings',
        'section_id' => 'gallery_settings',
        'position' => 'normal',
        'fields' => array(
            'sub_title' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'sub_title',
                'title' => __('Sub Title', 'lifeline'),
            ),
            'gallery' => array(
                'type' => 'gallery', //builtin fields include:
                'id' => 'gallery',
                'title' => __('Gallery', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
            'videos' => array(
                'type' => 'multi_text', //builtin fields include:
                'id' => 'videos',
                'title' => __('Videos', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
        )
    ),
    array('section_name' => 'Display Settings',
        'section_id' => 'gallery_display',
        'position' => 'normal',
        'fields' => array(
            'sidebar_pos' =>
            array(
                'type' => 'radio', //builtin fields include:
                'id' => 'sidebar_pos',
                'title' => __('Sidebar Postion', 'lifeline'),
                'options' => array('left' => 'Left', 'right' => 'Right'),
                'attributes' => array('style' => 'width:40%'),
            ),
            'sidebar' =>
            array(
                'type' => 'select', //builtin fields include:
                'id' => 'sidebar',
                'title' => __('Sidebar', 'lifeline'),
                'options' => sh_get_sidebars(),
            ),
            'top_image' => array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'help' => __("This is short help", 'lifeline'),
                'attributes' => array('placeholder' => __('Upload Page Top Image', 'lifeline')),
            ),
        ),
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
$settings['post'] = array(
    array('section_name' => 'Post Settings',
        'section_id' => 'post_Settings',
        'position' => 'normal',
        'fields' => array(
            'location' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'location',
                'title' => __('Location', 'lifeline'),
                'attributes' => array('placeholder' => __('Enter Location', 'lifeline')),
            ),
            'videos' => array(
                'type' => 'multi_text', //builtin fields include:
                'id' => 'videos',
                'title' => __('Videos', 'lifeline'),
            ),
            'format' => array(
                'type' => 'radio', //builtin fields include:
                'id' => 'format',
                'title' => __('Post Format', 'lifeline'),
                'options' => array('image' => 'Image', 'video' => 'Video', 'slider' => 'Slider'),
                'attributes' => array('style' => 'width:40%'),
            ),
        )
    ),
    array('section_name' => 'Display Settings',
        'section_id' => 'post_display',
        'position' => 'normal',
        'fields' => array(
            'sidebar_pos' =>
            array(
                'type' => 'radio', //builtin fields include:
                'id' => 'sidebar_pos',
                'title' => __('Sidebar Postion', 'lifeline'),
                'options' => array('left' => 'Left', 'right' => 'Right'),
                'attributes' => array('style' => 'width:40%'),
            ),
            'sidebar' =>
            array(
                'type' => 'select', //builtin fields include:
                'id' => 'sidebar',
                'title' => __('Sidebar', 'lifeline'),
                'options' => sh_get_sidebars(),
            ),
            'top_image' =>
            array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'attributes' => array('placeholder' => __('Upload Post Top Image', 'lifeline')),
            ),
            'hide_post_date' =>
            array(
                'type' => 'checkbox', //builtin fields include:
                'id' => 'hide_post_date',
                'title' => __('Hide Post Date', 'lifeline'),
                'desc' => '',
                'help' => __("Enable to hide post date", 'lifeline'),
            ),
            'hide_post_author' =>
            array(
                'type' => 'checkbox', //builtin fields include:
                'id' => 'hide_post_author',
                'title' => __('Hide Post Author', 'lifeline'),
                'desc' => '',
                'help' => __("Enable to hide post author", 'lifeline'),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
$settings['dict_team'] = array(
    array('section_name' => 'Features Settings',
        'section_id' => 'features_Settings',
        'position' => 'normal',
        'fields' => array(
            'name' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'name',
                'title' => __('Name', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
            'designation' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'designation',
                'title' => __('Designation', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
            'experience' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'experience',
                'title' => __('Experience', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
            'gallery' => array(
                'type' => 'gallery', //builtin fields include:
                'id' => 'gallery',
                'title' => __('Gallery', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
        )
    ),
    array('section_name' => 'Contact Settngs',
        'section_id' => 'contact_Settings',
        'position' => 'normal',
        'fields' => array(
            'email' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'email',
                'title' => __('Email', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
            'phone' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'phone',
                'title' => __('Phone', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
            'fb_link' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'fb_link',
                'title' => __('Facebook Link', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
            'gplus_link' => array(
                'type' => 'text', //builtin fields include:
                'id' => 'gplus_link',
                'title' => __('Google Plus Link', 'lifeline'),
                'class' => 'regular-text text-input',
            ),
        )
    ),
    array('section_name' => 'Top Image',
        'section_id' => 'top_img',
        'position' => 'normal',
        'fields' => array(
            'top_image' =>
            array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'attributes' => array('placeholder' => __('Upload Team Top Image', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
$settings['dict_services'] = array(
    array('section_name' => 'Servces Information',
        'section_id' => 'services_info',
        'position' => 'normal',
        'fields' => array(
            'font_awesome' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'font_awesome',
                'title' => __('Service Icon', 'lifeline'),
                'options' => sh_font_awesome(),
            ),
            'benifits' => array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'benifits',
                'title' => __('Benefits', 'lifeline'),
                'help' => 'Enter Comma Seperated Benifits',
            ),
        )
    ),
    array('section_name' => 'Display Options',
        'section_id' => 'services_display',
        'position' => 'normal',
        'fields' => array(
            'sidebar_pos' => array(
                'type' => 'radio', //builtin fields include:
                'id' => 'sidebar_pos',
                'title' => __('Sidebar Postion', 'lifeline'),
                'options' => array('left' => 'Left', 'right' => 'Right'),
                'attributes' => array('style' => 'width:40%'),
            ),
            'sidebar' => array(
                'type' => 'select', //builtin fields include:
                'id' => 'sidebar',
                'title' => __('Sidebar', 'lifeline'),
                'options' => sh_get_sidebars(),
            ),
            'top_image' =>
            array(
                'type' => 'upload', //builtin fields include:
                'id' => 'top_image',
                'title' => __('Top Image', 'lifeline'),
                'attributes' => array('placeholder' => __('Upload Services Top Image', 'lifeline')),
            ),
        )
    ),
    array('section_name' => 'SEO Settings',
        'section_id' => 'seo_settings',
        'position' => 'normal',
        'fields' => array(
            'meta_title' =>
            array(
                'type' => 'text', //builtin fields include:
                'id' => 'meta_title',
                'title' => __('Meta Title', 'lifeline'),
                'help' => __("Enter Meta Title.", 'lifeline'),
            ),
            'meta_desc' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_desc',
                'title' => __('Meta Description', 'lifeline'),
                'help' => __("Enter Meta Description.", 'lifeline'),
            ),
            'meta_keywords' =>
            array(
                'type' => 'textarea', //builtin fields include:
                'id' => 'meta_keywords',
                'title' => __('Meta Key Words', 'lifeline'),
                'help' => __("Enter Meta Key Words.", 'lifeline'),
            ),
        )
    ),
);
