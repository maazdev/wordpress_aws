<?php
define('DOMAIN', 'lifeline');
define('SH_VERSION', 'v3.0.1');
define('SH_ROOT', get_template_directory() . '/');

define('SH_URL', get_template_directory_uri() . '/');
define('LIFELINE_KEY', '!@#lifeline');
define('SH_DIR', dirname(__FILE__));
define('TH_NAME', 'Lifeline');
get_template_part('framework/loader');
if (in_array('bbpress/bbpress.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    get_template_part('framework/modules/bbpress_fix');
}
define('STRIPE_PRIVATE_KEY', sh_set(get_option('lifeline'), 'credit_card_secret_key'));
define('STRIPE_PUBLIC_KEY', sh_set(get_option('lifeline'), 'credit_card_publish_key'));

add_action('after_setup_theme', 'sh_theme_setup');
if (!session_id())
    session_start();

function sh_theme_setup() {
    global $wp_version;
//sh_create_donation_table();
    load_theme_textdomain('lifeline', get_template_directory() . '/languages');
    add_editor_style();
    add_theme_support('post-thumbnails');
    add_theme_support('woocommerce');
    add_theme_support('menus'); //Add menu support
    add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
    add_theme_support('widgets'); //Add widgets and sidebar support
    /** Register wp_nav_menus */
    add_theme_support("custom-header");
    add_theme_support("custom-background");
    add_theme_support('title-tag');



    if (function_exists('register_nav_menu')) {
        register_nav_menus(
                array(
                    /** Register Main Menu location header */
                    'main_menu' => __('Main Menu', 'lifeline'),
                    'footer_menu' => __('Footer Menu', 'lifeline'),
                    'responsive_menu' => __('Responsive Menu', 'lifeline'),
                )
        );
    }
    if (!isset($content_width))
        $content_width = 960;
    $ThumbSize = array('370x491', '1170x455', '370x252', '270x155', '570x570', '150x150', '570x184', '1170x312', '80x80', '470x318', '570x353');
    foreach ($ThumbSize as $v) {
        $explode = explode('x', $v);
        add_image_size($v, $explode[0], $explode[1], true);
    }
    if (isset($_POST['recurring_pp_submit'])) {
        if (defined('CPATH'))
            $paypal_res = require_once(CPATH . 'includes/pp_recurring/expresscheckout.php');
    }
}

function sh_widget_init() {
    register_widget('SH_people_reviews');
    register_widget('SH_Flickr');
    register_widget('SH_Contact_Us');
    register_widget('SH_News_Letter_Subscription');
    register_widget('SH_Galleries');
    register_widget('SH_Popular_Posts');
    register_widget('SH_Recent_Events');
    register_widget('SH_Video');
    register_widget('SH_Donate_Us');
    register_widget('sh_categories');
    register_widget('sh_instagram_Widget');
    global $wp_registered_sidebars;
    register_sidebar(array(
        'name' => __('Default Sidebar', 'lifeline'),
        'id' => 'default-sidebar',
        'description' => __('Widgets in this area will be shown on the right-hand side.', 'lifeline'),
        'class' => '',
        'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="sidebar-title"><h4>',
        'after_title' => '</h4></div>'
    ));
    register_sidebar(array(
        'name' => __('Blog Listing', 'lifeline'),
        'id' => 'blog-sidebar',
        'description' => __('Widgets in this area will be shown on the right-hand side.', 'lifeline'),
        'class' => '',
        'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="sidebar-title"><h4>',
        'after_title' => '</h4></div>'
    ));
    $footer_columns = sh_set(get_option('lifeline'), 'column_type');
    $footer_columns = $footer_columns ? $footer_columns : 'col-md-3';

    register_sidebar(array(
        'name' => __('Footer Sidebar', 'lifeline'),
        'id' => 'footer-sidebar',
        'description' => __('Widgets in this area will be shown on the right-hand side.', 'lifeline'),
        'class' => 'quick-menu',
        'before_widget' => '<div class= ' . $footer_columns . '>',
        'after_widget' => '</div>',
        'before_title' => '<div class="footer-widget-title"><h4>',
        'after_title' => '</h4></div>'
    ));
    $sidebars = sh_set(get_option('lifeline'), 'dynamic_sidebars'); //printr($sidebars);
    foreach (array_filter((array) $sidebars) as $sidebar) {
        register_sidebar(array(
            'name' => $sidebar,
            'id' => bistro_slug($sidebar),
            'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
            'after_widget' => "</div>",
            'before_title' => '<div class="sidebar-title"><h4>',
            'after_title' => '</h4></div>',
        ));
    }
    update_option('wp_registered_sidebars', $wp_registered_sidebars);
}

add_action('widgets_init', 'sh_widget_init');

function sh_custom_header() {
    $settings = get_option('lifeline');
    $HeaderName = ( sh_set($settings, 'custom_header') !== 'dafault' ) ? sh_set($settings, 'custom_header') : '';

//if(is_page(1160)): $HeaderName = NULL; $HeaderName = 'header-counter'; endif;
    get_header($HeaderName);
}

function get_price_html($price = '') {
    global $product;
// Ensure variation prices are synced with variations
    if ($product->min_variation_price === '' || $product->min_variation_regular_price === '' || $product->price === '')
//$product->variable_product_sync();
// Get the price
        if ($product->price > 0) {
            if ($product->is_on_sale() && isset($product->min_variation_price) && $product->min_variation_regular_price !== $product->get_price()) {
                if (!$product->min_variation_price || $product->min_variation_price !== $product->max_variation_price)
                    $price .= $product->get_price_html_from_text();
                $price .= $product->get_price_html_from_to($product->min_variation_regular_price, $product->get_price());
                $price = apply_filters('woocommerce_variable_sale_price_html', $price, $product);
            }
            else {
                if ($product->min_variation_price !== $product->max_variation_price)
                    $price .= $product->get_price_html_from_text();
                $price .= woocommerce_price($product->get_price());
                $price = apply_filters('woocommerce_variable_price_html', $price, $product);
            }
        }
        elseif ($product->price === '') {
            $price = apply_filters('woocommerce_variable_empty_price_html', '', $product);
        } elseif ($product->price == 0) {
            if ($product->is_on_sale() && isset($product->min_variation_regular_price) && $product->min_variation_regular_price !== $product->get_price()) {
                if ($product->min_variation_price !== $product->max_variation_price)
                    $price .= $product->get_price_html_from_text();

                $price .= $product->get_price_html_from_to($product->min_variation_regular_price, __('Free!', 'lifeline'));

                $price = apply_filters('woocommerce_variable_free_sale_price_html', $price, $product);
            }
            else {
                if ($product->min_variation_price !== $product->max_variation_price)
                    $price .= $product->get_price_html_from_text();
                $price .= __('Free!', 'lifeline');
                $price = apply_filters('woocommerce_variable_free_price_html', $price, $product);
            }
        }
    return apply_filters('woocommerce_get_price_html', $price, $product);
}

function donation_box() {
    $paypal = $GLOBALS['_sh_base']->donation;
    echo '<div id="hidden_popup_donation_btn_click" style="display: none;"></div>';
    echo '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    if (isset($_GET['recurring_pp_return']) && $_GET['recurring_pp_return'] == 'return') {
        if (defined('CPATH'))
            $paypal_res = require_once(CPATH . 'includes/pp_recurring/order_confirm.php');
        ?>
        <div class="donate-popup"><?php echo $paypal_res ?></div>
        <script>
            jQuery(document).ready(function ($) {
                var popup = jQuery("div.confirm_popup");
                jQuery(popup).parent().css({"border-top": "none"});
                jQuery('a[data-target="#myModal"]').trigger("click");
            });
        </script>
        <?php
    }
    echo '</div>';
}

require_once( get_template_directory() . '/envato_setup/envato_setup.php' );
add_filter('sh_theme_setup_wizard_username', 'sh_set_theme_setup_wizard_username', 10);
if( ! function_exists('sh_set_theme_setup_wizard_username') ){
    function sh_set_theme_setup_wizard_username($username){
        return 'dtbaker';
    }
}

add_filter('sh_theme_setup_wizard_oauth_script', 'sh_set_theme_setup_wizard_oauth_script', 10);
if( ! function_exists('sh_set_theme_setup_wizard_oauth_script') ){
    function sh_set_theme_setup_wizard_oauth_script($oauth_url){
        return 'http://api.webinane.com/envato/api/server-script.php';
    }
}

add_action('wp_ajax_theme-install-demo-data', 'theme_ajax_install_dummy_data');

function theme_ajax_install_dummy_data() {
    require_once('framework/helpers/importer.php');
    sh_xml_importer();
    die();
}

remove_filter('nav_menu_description', 'strip_tags');

function sh_setup_nav_menu_item($menu_item) {
    if (isset($menu_item->post_type)) {
        if ('nav_menu_item' == $menu_item->post_type) {
            $menu_item->description = apply_filters('nav_menu_description', $menu_item->post_content);
        }
    }

    return $menu_item;
}

add_filter('wp_setup_nav_menu_item', 'sh_setup_nav_menu_item');

//responsive menu
function sh_responsive_menu() {
    $settings = get_option('lifeline');
    wp_enqueue_script(array('perfect-scrollbar-jquery', 'perfect-scrollbar'));
    ?>
    <div class="responsive-header">
        <?php if (sh_set($settings, 'sh_responsive_header_top_bar')): ?>
            <div class="responsive-topbar">		
                <div class="responsive-topbar-info">
                    <ul>
                        <?php echo (sh_set($settings, 'responsive_header_address')) ? '<li><i class="icon-home"></i> ' . sh_set($settings, 'responsive_header_address') . '</li>' : ''; ?>
                        <?php echo (sh_set($settings, 'responsive_header_phone_number')) ? '<li><i class="icon-phone"></i> ' . sh_set($settings, 'responsive_header_phone_number') . '</li>' : ''; ?>
                        <?php echo (sh_set($settings, 'responsive_header_email_address')) ? '<li><i class="icon-envelope"></i> ' . sh_set($settings, 'responsive_header_email_address') . '</li>' : ''; ?>
                    </ul>
                    <?php if (sh_set($settings, 'sh_show_responsive_soical_icons') == 'true'): ?>
                        <div class="container">
                            <div class="responsive-socialbtns">
                                <ul>
                                    <?php echo ( sh_set($settings, 'contact_rss') ) ? '<li><a target="_blank" href="' . sh_set($settings, 'contact_rss') . '" title=""><i class="icon-rss"></i></a></li>' : ''; ?>
                                    <?php echo ( sh_set($settings, 'contact_gplus') ) ? '<li><a target="_blank" href="' . sh_set($settings, 'contact_gplus') . '" title=""><i class="icon-google-plus"></i></a></li>' : ''; ?>
                                    <?php echo ( sh_set($settings, 'contact_facebook') ) ? '<li><a target="_blank" href="' . sh_set($settings, 'contact_facebook') . '" title=""><i class="icon-facebook"></i></a></li>' : ''; ?>
                                    <?php echo ( sh_set($settings, 'contact_twitter') ) ? '<li><a target="_blank" href="' . sh_set($settings, 'contact_twitter') . '" title=""><i class="icon-twitter"></i></a></li>' : ''; ?>
                                    <?php echo ( sh_set($settings, 'contact_linkedin') ) ? '<li><a target="_blank" href="' . sh_set($settings, 'contact_linkedin') . '" title=""><i class="icon-linkedin"></i></a></li>' : ''; ?>
                                    <?php echo ( sh_set($settings, 'contact_pintrest') ) ? '<li><a target="_blank" href="' . sh_set($settings, 'contact_pintrest') . '" title=""><i class="icon-pinterest"></i></a></li>' : ''; ?>
                                    <?php echo ( sh_set($settings, 'contact_instagram') ) ? '<li><a target="_blank" href="' . sh_set($settings, 'contact_instagram') . '" title=""><i class="fa fa-instagram"></i></a></li>' : ''; ?>
                                    <?php echo ( sh_set($settings, 'contact_youtube') ) ? '<li><a target="_blank" href="' . sh_set($settings, 'contact_youtube') . '" title=""><i class="fa fa-youtube"></i></a></li>' : ''; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="responsive-logomenu">
            <div class="container">
                <?php
                if (sh_set($settings, 'responsive_logo_text_status') === 'true') {
                    $LogoStyle = sh_get_font_settings(
                            array('respnosive_logo_text_font_size' => 'font-size',
                        'responsive_logo_text_font_family' => 'font-family',
                        'responsive_logo_text_font_style' => 'font-style',
                        'responsive_logo_text_color' => 'color'), ' style="', '"');
                    $Logo = $settings['responsive_logo_text'];
                } else {
                    $LogoStyle = '';
                    $LogoImageStyle = ( sh_set($settings, 'responsive_logo_width') || sh_set($settings, 'responsive_logo_height') ) ? ' style="' : '';
                    $LogoImageStyle .= ( sh_set($settings, 'responsive_logo_width') ) ? ' width:' . sh_set($settings, 'responsive_logo_width') . 'px;' : '';
                    $LogoImageStyle .= ( sh_set($settings, 'responsive_logo_height') ) ? ' height:' . sh_set($settings, 'responsive_logo_height') . 'px;' : '';
                    $LogoImageStyle .= ( sh_set($settings, 'responsive_logo_width') || sh_set($settings, 'responsive_logo_height') ) ? '"' : '';
                    $Logo = '<img src="' . sh_set($settings, 'responsive_logo_image') . '" alt=""' . $LogoImageStyle . ' />';
                }
                ?>
                <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"<?php echo $LogoStyle; ?>>
                    <?php if (sh_set($settings, 'responsive_logo_text_status') === 'true')  ?> <h1 <?php echo $LogoStyle; ?>>
                        <?php echo $Logo; ?>
                        <?php if (sh_set($settings, 'responsive_logo_text_status') === 'true')  ?> </h1>
                </a>
                <?php
                if (sh_set($settings, 'responsive_logo_text_status') === 'true' && sh_set($settings, 'responsive_site_salogan')) {
                    $SaloganStyle = sh_get_font_settings(array('responsive_salogan_font_size' => 'font-size', 'responsive_salogan_font_family' => 'font-family', 'responsive_salogan_font_style' => 'font-style'), ' style="', '"');
                    echo '<p' . $SaloganStyle . '>' . sh_set($settings, 'responsive_site_salogan') . '</p>';
                }
                ?>
                <span class="menu-btn"><i class="icon-list"></i></span>
            </div>
        </div>
        <div class="responsive-menu">
            <span class="close-btn"><i class="fa fa-close"></i></span>
            <?php wp_nav_menu(array('theme_location' => 'responsive_menu', 'menu_class' => '', 'container' => null)); ?>
        </div> 
        <?php
        if (sh_set($settings, 'sh_show_responsive_donate_btn') == 'true'):

            if (sh_set($settings, 'sh_donate_btn_link')) {
                ?>
                <a  href="<?php echo esc_url(sh_set($settings, 'sh_donate_btn_link')); ?>" data-type="general" class="responsive-donate" title="" ><?php echo sh_set($settings, 'sh_show_donate_btn_txt') ?></a>

            <?php }
            
            else { ?>
                
                <a data-toggle="modal" data-target="#myModal" data-url="<?php echo get_permalink() ?>" data-type="general" class="btn-don responsive-donate" href="#" title=""><?php echo sh_set($settings, 'sh_show_donate_btn_txt') ?></a>
            <?php } ?>  

        <?php endif; ?>

    </div><!--Responsive header-->  

    <?php
}

function sh_woo_pages($page_id) {
    $pages = array(
        get_option('woocommerce_shop_page_id'),
        get_option('woocommerce_cart_page_id'),
        get_option('woocommerce_checkout_page_id'),
        get_option('woocommerce_pay_page_id'),
        get_option('woocommerce_thanks_page_id'),
        get_option('woocommerce_myaccount_page_id'),
        get_option('woocommerce_edit_address_page_id'),
        get_option('woocommerce_view_order_page_id'),
        get_option('woocommerce_terms_page_id')
    );
    return ( in_array($page_id, $pages) ) ? 'true' : 'false';
}

function sh_search_filter($query) {
    if (!$query->is_admin && $query->is_search) {
        $query->set('post_type', array('post', 'dict_testimonials', 'dict_causes', 'dict_project', 'dict_event', 'dict_portfolio', 'dict_gallery', 'dict_team', 'dict_services'));
    }
    return $query;
}

add_filter('pre_get_posts', 'sh_search_filter');

function vp_get_posts_custom($post_tyep) {
    $args = array(
        'post_type' => $post_tyep,
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );

    $result = array();
    $my_query = null;
    $my_query = new WP_Query($args);
    if ($my_query->have_posts()) {
        foreach ($my_query->posts as $key => $value):
            $result[$value->ID] = $value->post_title;
        endforeach;
    }
    return $result;
    wp_reset_query();
}

add_action('wp_ajax_sh_donation_popup_ajax', 'sh_donation_popup_ajax');
add_action('wp_ajax_nopriv_sh_donation_popup_ajax', 'sh_donation_popup_ajax');

function sh_donation_popup_ajax() {
    //check_ajax_referer(LIFELINE_KEY, 'LIFELINE_popupkey');

    if (isset($_POST) && sh_set($_POST, 'action') == 'sh_donation_popup_ajax') {

        ob_start();
        if (isset($_SESSION['donation_type_popup'])) {
            unset($_SESSION['donation_type_popup']);
        } elseif (isset($_SESSION['donation_id_popup'])) {
            unset($_SESSION['donation_id_popup']);
        }
        if (!session_id())
            session_start();
        $r = session_id();
        $paypal = $GLOBALS['_sh_base']->donation;
        $http = (is_ssl()) ? 'https' : 'http';
        $return_url = 'http://localhost/one_click_importer/paypal-ipn/';
        $redirect_url = $_SESSION['redirect_uri'] = esc_url(sh_set($_POST, 'url'));
        $temp_array = array('type' => esc_attr(sh_set($_POST, 'types')), 'id' => esc_attr(sh_set($_POST, 'id')), 'redirect' => $redirect_url);
        update_option('temp_donation_recuring' . $r, $temp_array);

        if (esc_attr(sh_set($_POST, 'types')) == 'post') {

            $_SESSION['donation_id_popup'] = esc_attr(sh_set($_POST, 'id'));
            $_SESSION['donation_type_popup'] = 'dict_causes';

            $args = array(
                'post_type' => "dict_causes",
                'p' => sh_set($_POST, 'id'),
            );
            $query = new WP_Query($args);
            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
                    $settings = get_post_meta(get_the_ID(), '_dict_causes_settings', true);
                    $symbol = sh_set($settings, 'currency_symbol', '$');
                    $sh_currency_code = sh_set($settings, 'currency_code', 'USD');
                    $c_needed = sh_set($settings, 'donation_needed');
                    $c_collect = sh_set($settings, 'donation_collected');
                    $c_percent = ($c_needed) ? (int) str_replace(',', '', $c_collect) / (int) str_replace(',', '', $c_needed) : 0;
                    $c_donation_percentage = round($c_percent * 100, 2, PHP_ROUND_HALF_UP);
                }
            }
        } else if (esc_attr(sh_set($_POST, 'types')) == 'project') {
            $_SESSION['donation_id_popup'] = esc_attr(sh_set($_POST, 'id'));
            $_SESSION['donation_type_popup'] = 'dict_project';

            $args = array(
                'post_type' => "dict_project",
                'p' => esc_attr(sh_set($_POST, 'id')),
            );
            $query = new WP_Query($args);
            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
                    $settings = get_post_meta(get_the_ID(), '_dict_project_settings', true);
                    $symbol = sh_set($settings, 'currency_symbol', '$');
                    $sh_currency_code = sh_set($settings, 'currency_code', 'USD');
                    $p_needed = sh_set($settings, 'amount_needed');
                    $p_collect = sh_set($settings, 'spent_amount');
                    $p_percent = ($p_needed) ? (int) str_replace(',', '', $p_needed) / (int) str_replace(',', '', $p_collect) : 0;
                    $p_donation_percentage = round($p_percent * 100, 2, PHP_ROUND_HALF_UP);
                }
            }
        } else {
            $donation_data = get_option('lifeline');
            $symbol = (sh_set($donation_data, 'paypal_currency')) ? sh_set($donation_data, 'paypal_currency') : '$';
            $percent = (sh_set($donation_data, 'paypal_target')) ? (int) str_replace(',', '', sh_set($donation_data, 'paypal_raised')) / (int) str_replace(',', '', sh_set($donation_data, 'paypal_target')) : 0;
            $donation_percentage = $percent * 100;
            $settings = get_option('lifeline');
            $sh_currency_code = sh_set($settings, 'currency_code', 'USD');
        }

        $Settings = get_option('lifeline');
        $value = sh_set($Settings, 'transactions_detail');
        ?>
        <div class="donate-popup">
            <?php if (esc_attr((sh_set($_POST, 'types') == 'project'))): ?>
                <div class="cause-bar">
                    <div class="cause-box">
                        <h3>
                            <span><?php echo $symbol ?></span>
                            <?php echo $p_needed ?>
                        </h3>
                        <i><?php _e('NEEDED DONATION', 'lifeline') ?></i>
                    </div>
                    <div class="cause-progress">
                        <div class="progress-report">
                            <h6><?php _e('PHASES', 'lifeline') ?></h6>
                            <span><?php echo $p_donation_percentage ?>%</span>
                            <div class="progress pattern">
                                <div class="progress-bar" style="width: <?php echo $p_donation_percentage ?>%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="cause-box">
                        <h3>
                            <span><?php echo $symbol ?></span>
                            <?php echo $p_collect ?></h3>
                        <i><?php _e('COLLECTED DONATION', 'lifeline') ?></i>
                    </div>
                    <div class="cause-box donate-drop-btn">
                        <h4><?php _e('DONATE NOW', 'lifeline') ?></h4>
                    </div>
                </div>
            <?php elseif (esc_attr(sh_set($_POST, 'types') == 'post')): ?>
                <div class="cause-bar">
                    <div class="cause-box">
                        <h3>
                            <span><?php echo $symbol ?></span>
                            <?php echo $c_needed ?>
                        </h3>
                        <i><?php _e('NEEDED DONATION', 'lifeline') ?></i>
                    </div>
                    <div class="cause-progress">
                        <div class="progress-report">
                            <h6><?php _e('PHASES', 'lifeline') ?></h6>
                            <span><?php echo $c_donation_percentage ?>%</span>
                            <div class="progress pattern">
                                <div class="progress-bar" style="width: <?php echo $c_donation_percentage ?>%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="cause-box">
                        <h3>
                            <span><?php echo $symbol ?></span>
                            <?php echo $c_collect ?></h3>
                        <i><?php _e('COLLECTED DONATION', 'lifeline') ?></i>
                    </div>
                    <div class="cause-box donate-drop-btn">
                        <h4><?php _e('DONATE NOW', 'lifeline') ?></h4>
                    </div>
                </div>
            <?php else: ?>
                <div class="cause-bar">
                    <div class="cause-box">
                        <h3>
                            <span><?php echo $symbol ?></span>
                            <?php echo sh_set($donation_data, 'paypal_target') ?>
                        </h3>
                        <i><?php _e('NEEDED DONATION', 'lifeline') ?></i>
                    </div>
                    <div class="cause-progress">
                        <div class="progress-report">
                            <h6><?php _e('PHASES', 'lifeline') ?></h6>
                            <span><?php echo $donation_percentage ?>%</span>
                            <div class="progress pattern">
                                <div class="progress-bar" style="width: <?php echo $donation_percentage ?>%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="cause-box">
                        <h3>
                            <span><?php echo $symbol ?></span>
                            <?php echo sh_set($donation_data, 'paypal_raised') ?></h3>
                        <i><?php _e('COLLECTED DONATION', 'lifeline') ?></i>
                    </div>
                    <div class="cause-box donate-drop-btn">
                        <h4><?php _e('DONATE NOW', 'lifeline') ?></h4>
                    </div>
                </div>
            <?php endif; ?>
            <div class="donate-drop-down">
                <div class="recursive-periods" align="center">
                    <?php
                    $translated = array(
                        'One Time' => __('One Time', 'lifeline'),
                        'daily' => __('Daily', 'lifeline'),
                        'weekly' => __('Weekly', 'lifeline'),
                        'fortnightly' => __('Fortnightly', 'lifeline'),
                        'monthly' => __('Monthly', 'lifeline'),
                        'quarterly' => __('Quarterly', 'lifeline'),
                        'half_year' => __('Half Year', 'lifeline'),
                        'yearly' => __('Yearly', 'lifeline'),
                    );
                    if ($value) {
                        foreach ($value as $val) {
                            echo '<a data-symbol="' . $sh_currency_code . '" data-currency="' . $symbol . '" style="cursor:pointer;">' . $translated[$val] . '</a>';
                        }
                    }
                    ?>
                </div>
                <div class="amount-btns">
                    <?php
                    if (intval(sh_set($Settings, 'pop_up_1st_value')) != '')
                        echo '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_1st_value') . '</span></a>';
                    if (intval(sh_set($Settings, 'pop_up_2nd_value')) != '')
                        echo '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_2nd_value') . '</span></a>';
                    if (intval(sh_set($Settings, 'pop_up_3rd_value')) != '')
                        echo '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_3rd_value') . '</span></a>';
                    if (intval(sh_set($Settings, 'pop_up_4th_value')) != '')
                        echo '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_4th_value') . '</span></a>';
                    if (intval(sh_set($Settings, 'pop_up_5th_value')) != '')
                        echo '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_5th_value') . '</span></a>';
                    if (intval(sh_set($Settings, 'pop_up_6th_value')) != '')
                        echo '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_6th_value') . '</span></a>';
                    if (intval(sh_set($Settings, 'pop_up_7th_value')) != '')
                        echo '<a style="cursor:pointer;">' . $symbol . '<span>' . sh_set($Settings, 'pop_up_7th_value') . '</span></a>';
                    ?>
                </div>
                <div class="payment-method">
                    <div class="payment-choices">
                        <?php if (sh_set(get_option('lifeline'), 'enable_paypal') == 'true'): ?><a class="paypal-donation" href="javascript:void(0)" title=""><?php _e('PAYPAL', 'lifeline') ?></a><?php endif; ?>
                        <?php if (sh_set(get_option('lifeline'), 'enable_stripe') == 'true'): ?><a class="credit-card" href="javascript:void(0)" title=""><?php _e('CREDIT CARD', 'lifeline') ?></a><?php endif; ?>
                        <?php if (sh_set(get_option('lifeline'), 'enable_checkout2') == 'true'): ?><a class="checkout2" href="javascript:void(0)" title=""><?php _e('2Checkout', 'lifeline') ?></a><?php endif; ?>
                        <?php if (sh_set(get_option('lifeline'), 'enable_braintree') == 'true'): ?><a class="braintree" href="javascript:void(0)" title=""><?php _e('Braintree', 'lifeline') ?></a><?php endif; ?>
	                    <?php if (sh_set(get_option('lifeline'), 'enable_payumoney') == 'true'): ?><a class="payumoney" href="javascript:void(0)" title=""><?php _e('PayUMoney', 'lifeline') ?></a><?php endif; ?>
	                    <?php if (sh_set(get_option('lifeline'), 'enable_quickpay') == 'true'): ?><a class="quickpay" href="javascript:void(0)" title=""><?php _e('QuickPay', 'lifeline') ?></a><?php endif; ?>
                    </div>
                    <?php if (sh_set(get_option('lifeline'), 'enable_paypal') == 'true' || sh_set(get_option('lifeline'), 'enable_stripe') == 'true'): ?>
                        <div class="other-amount donner credit-card-options">
                            <div id="sh_to_errors" class="col-md-12">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input id="donner_name" name="donner_name" type="text" placeholder="<?php _e('ENTER YOUR NAME PLEASE', 'lifeline'); ?>" autocomplete="off" />
                                </div>

                                <div class="col-md-6">
                                    <input id="donner_email" name="donner_email" type="text" placeholder="<?php _e('ENTER YOUR EMAIL PLEASE', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                            </div>

                        </div>
                    <?php endif; ?>
                    <form id="credit_card_form" class="credit-card-options">
                        <div class="other-amount card">
                            <div class="row">
                                <div id="payment-errors"></div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="amount" name="amount" type="text" placeholder="<?php _e('ENTER YOUR AMOUNT PLEASE', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="card_number" type="text" placeholder="<?php _e('Enter Your 16 digit Card Number', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-4 col-md-offset-2">
                                    <select class="select" id="card-mnth">
                                        <?php
                                        $mnth = range(1, 12);
                                        foreach ($mnth as $m) {
                                            if ($m <= 9) {
                                                echo '<option value="0' . $m . '">' . $m . '</option>';
                                            } else {
                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-md-offset-0">
                                    <select class="select" id="card-year">
                                        <?php
                                        $mnth = range(2015, 2025);
                                        foreach ($mnth as $m) {
                                            echo '<option value="' . $m . '">' . $m . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="cvc" type="text" placeholder="<?php _e('Card Verification Number', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-12">
                                    <input id="submitBtn" type="submit" value="<?php _e('DONATE NOW', 'lifeline'); ?>" />
                                </div>
                            </div>
                        </div>
                    </form>

                    <form id="checkout2_form" class="checkout2-options">
                        <div class="other-amount checkout2">
                            <div class="row">
                                <div id="payment-errors"></div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="amount" name="amount" type="text" placeholder="<?php _e('ENTER YOUR AMOUNT PLEASE', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <?php
                                    if (sh_country_list()) {
                                        echo '<select class="select" id="checkout2_country">';
                                        foreach (sh_country_list() as $k => $v) {
                                            echo '<option id="' . $k . '">' . $v . '</option>';
                                        }
                                        echo '</select>';
                                    }
                                    ?>
                                </div>

                                <div class="col-md-8 col-md-offset-2">
                                    <input id="chekcout2_address" type="text" placeholder="<?php _e('Enter Your Address', 'lifeline'); ?>" autocomplete="off" />
                                </div>

                                <div class="col-md-4 col-md-offset-2">
                                    <input id="chekcout2_city" type="text" placeholder="<?php _e('Enter Your City', 'lifeline'); ?>" autocomplete="off" />
                                </div>

                                <div class="col-md-4">
                                    <input id="chekcout2_state" type="text" placeholder="<?php _e('Enter Your State', 'lifeline'); ?>" autocomplete="off" />
                                </div>

                                <div class="col-md-4 col-md-offset-2">
                                    <input id="chekcout2_zip_code" type="text" placeholder="<?php _e('Enter Your Zip Code', 'lifeline'); ?>" autocomplete="off" />
                                </div>

                                <div class="col-md-4">
                                    <input id="chekcout2_contact_no" type="text" placeholder="<?php _e('Enter Your Phone Number', 'lifeline'); ?>" autocomplete="off" />
                                </div>

                                <div class="col-md-8 col-md-offset-2">
                                    <input id="card_number" type="text" placeholder="<?php _e('Enter Your 16 digit Card Number', 'lifeline'); ?>" autocomplete="off" />
                                </div>

                                <div class="col-md-4 col-md-offset-2">
                                    <select class="select" id="card-mnth">
                                        <?php
                                        $mnth = range(1, 12);
                                        foreach ($mnth as $m) {
                                            if ($m <= 9) {
                                                echo '<option value="0' . $m . '">' . $m . '</option>';
                                            } else {
                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-md-offset-0">
                                    <select class="select" id="card-year">
                                        <?php
                                        $mnth = range(2015, 2025);
                                        foreach ($mnth as $m) {
                                            echo '<option value="' . $m . '">' . $m . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="cvc" type="text" placeholder="<?php _e('Card Verification Number', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-12">
                                    <input id="submitBtn" type="submit" value="<?php _e('DONATE NOW', 'lifeline'); ?>" />
                                </div>
                            </div>
                        </div>
                    </form>

                    <form id="braintree_form" class="braintree-options">
                        <div class="other-amount braintree">
                            <div class="row">
                                <div id="payment-errors"></div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="amount" name="amount" type="text" placeholder="<?php _e('ENTER YOUR AMOUNT PLEASE', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="card_number" type="text" placeholder="<?php _e('Enter Your 16 digit Card Number', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-4 col-md-offset-2">
                                    <select class="select" id="card-mnth">
                                        <?php
                                        $mnth = range(1, 12);
                                        foreach ($mnth as $m) {
                                            if ($m <= 9) {
                                                echo '<option value="0' . $m . '">' . $m . '</option>';
                                            } else {
                                                echo '<option value="' . $m . '">' . $m . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-md-offset-0">
                                    <select class="select" id="card-year">
                                        <?php
                                        $mnth = range(2015, 2025);
                                        foreach ($mnth as $m) {
                                            echo '<option value="' . $m . '">' . $m . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="cvc" type="text" placeholder="<?php _e('Card Verification Number', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-12">
                                    <input id="submitBtn" type="submit" value="<?php _e('DONATE NOW', 'lifeline'); ?>" />
                                </div>
                            </div>
                        </div>
                    </form>

                    <form id="payumoney_form" class="payumoney-options" action="https://test.payu.in/_payment" method="post">
		                <?php
		                $settings = get_option('lifeline');
		                $payumoney_mode = sh_set($settings, 'payumoney_mode');
		                $payumoney_key = sh_set($settings, 'payumoney_key');
		                $payumoney_salt = sh_set($settings, 'payumoney_salt');
		                $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		                $hashSequence = $payumoney_key|$txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|$payumoney_salt;
		                $hash = hash("sha512", $hashSequence);
		                $surl = 'success.php';
		                $furl = 'failure.php';
		                ?>
                        <div class="other-amount payumoney">
                            <div class="row">
                                <div id="payment-errors"></div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="amount" name="amount" type="text" placeholder="<?php _e('Enter Your Amount Please', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="phone" name="phone" type="text" placeholder="<?php _e('Phone Number', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <input id="productinfo" name="productinfo" type="text" placeholder="<?php _e('Product Info', 'lifeline'); ?>" autocomplete="off" />
                                </div>
                                <input type="hidden" id="payumoney_mode" name="payumoney_mode" value="<?php echo $payumoney_mode ?>" />
                                <input type="hidden" id="payumoney_key" name="key" value="<?php echo $payumoney_key ?>" />
                                <input type="hidden" id="payumoney_salt" name="payumoney_salt" value="<?php echo $payumoney_salt ?>" />
                                <input type="hidden" id="hash" name="hash" value="<?php echo $hash ?>"/>
                                <input type="hidden" id="txnid" name="txnid" value="<?php echo $txnid ?>" />
                                <input type="hidden" id="surl" name="surl" value="<?php echo $surl ?>" />
                                <input type="hidden" id="furl" name="furl" value="<?php echo $furl ?>" />
                                <input id="service_provider" type="hidden" name="service_provider" value="payu_paisa" />
                                <div class="col-md-12">
                                    <input id="submitBtn" type="submit" value="<?php _e('DONATE NOW', 'lifeline'); ?>" />
                                </div>
                            </div>
                        </div>
                    </form>

	                <?php
	                $settings = get_option('lifeline');
	                $quickpay_currency_code = sh_set($settings, 'currency_code');
	                $quickpay_mode = sh_set($settings, 'quickpay_mode');
	                $quickpay_merchant_id = sh_set($settings, 'quickpay_merchant_id');
	                $quickpay_agreement = sh_set($settings, 'quickpay_agreement');
	                $quickpay_api = sh_set($settings, 'quickpay_api');
	                $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	                $quickpay_continueurl = sh_set($settings, 'quickpay_continueurl');
	                $quickpay_cancelurl = sh_set($settings, 'quickpay_cancelurl');
	                $quickpay_callbackurl = sh_set($settings, 'quickpay_callbackurl');
                    $quickpay_action = 'https://payment.quickpay.net';
	                function sign($params, $api_key) {
		                $flattened_params = flatten_params($params);
		                ksort($flattened_params);
		                $base = implode(" ", $flattened_params);

		                return hash_hmac("sha256", $base, $api_key);
	                }

	                function flatten_params($obj, $result = array(), $path = array()) {
		                if (is_array($obj)) {
			                foreach ($obj as $k => $v) {
				                $result = array_merge($result, flatten_params($v, $result, array_merge($path, array($k))));
			                }
		                } else {
			                $result[implode("", array_map(function($p) { return "[{$p}]"; }, $path))] = $obj;
		                }

		                return $result;
	                }

	                $params = array(
		                "version"      => "v10",
		                "merchant_id"  => $quickpay_merchant_id,
		                "agreement_id" => $quickpay_agreement,
		                "order_id"     => $txnid,
		                "amount"       => 1000,
		                "currency"     => $quickpay_currency_code,
		                "continueurl"  => $quickpay_continueurl,
		                "cancelurl"    => $quickpay_cancelurl,
		                "callbackurl"  => $quickpay_callbackurl,
                        "autocapture"  => 1,
                        "payment_method" => "creditcard",
	                );

	                $params["checksum"] = sign($params, "$quickpay_api");
	                //printr($quickpay_currency_code);
	                ?>

                    <form id="quickpay_form" class="quickpay-options" method="POST" action="<?php echo $quickpay_action; ?>">
                        <input type="hidden" name="version" value="v10">
                        <input type="hidden" name="merchant_id" value="<?php echo $quickpay_merchant_id; ?>">
                        <input type="hidden" name="agreement_id" value="<?php echo $quickpay_agreement; ?>">
                        <input type="hidden" name="order_id" value="<?php echo $txnid; ?>">
                        <input type="hidden" name="amount" value="1000">
                        <input type="hidden" name="currency" value="<?php echo $quickpay_currency_code; ?>">
                        <input type="hidden" name="continueurl" value="<?php echo $quickpay_continueurl; ?>">
                        <input type="hidden" name="cancelurl" value="<?php echo $quickpay_cancelurl; ?>">
                        <input type="hidden" name="callbackurl" value="<?php echo $quickpay_callbackurl; ?>">
                        <input type="hidden" name="autocapture" value="1">
                        <input type="hidden" name="payment_methods" value="creditcard">
                        <input type="hidden" name="checksum" value="<?php echo $params["checksum"]; ?>">
                        <div class="col-md-12">
                            <input id="submitBtn" type="submit" value="<?php _e('DONATE NOW', 'lifeline'); ?>" />
                        </div>

                    </form>

                    <script>
                        jQuery(document).ready(function ($) {
                            $(".select").select2();
                        });
                    </script>
                    <div class="paypal-donaiton-box">
                        <div class="other-amount paypal">
                            <?php echo $paypal->button(array('currency_code' => $sh_currency_code, 'item_name' => get_bloginfo('name'), 'return' => $return_url)) ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        echo $output;
    }
    exit;
}

add_action('wp_ajax_sh_paypal_donner_before', 'sh_paypal_donner_before');
add_action('wp_ajax_nopriv_sh_paypal_donner_before', 'sh_paypal_donner_before');

function sh_paypal_donner_before() {
    if (isset($_POST) && sh_set($_POST, 'action') == 'sh_paypal_donner_before') {
        if (!session_id()) {
            session_start();
        }
        $r = session_id();
        $temp = array('don_name' => sh_set($_POST, 'don_name'), 'don_email' => sh_set($_POST, 'don_email'));
        if (update_option('temp_donor_info' . $r, $temp)) {
            echo '1';
        } else {
            echo '0';
        }
        exit;
    }
}

function lif_vd_details($url) {
    $host = explode('.', str_replace('www.', '', strtolower(parse_url($url, PHP_URL_HOST))));
    $host = isset($host[0]) ? $host[0] : $host;
    $videos = array();

    switch ($host) {
        case 'vimeo':


            $video_id = substr(parse_url($url, PHP_URL_PATH), 1);
            $video_track = explode('/', $video_id);
            $content = wp_remote_get("https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/" . end($video_track));
            $hash = json_decode(sh_set($content, 'body'));

            if ($hash != '') {
                return array(
                    'host' => 'vimeo',
                    'provider' => 'Vimeo',
                    'title' => sh_set($hash, 'title'),
                    'description' => str_replace(array("
", "
", "
"), NULL, sh_set($hash, 'description')),
                    'description_nl2br' => str_replace(array("\n", "\r", "\r\n", "\n\r"), NULL, sh_set($hash, 'description')),
                    'thumbnail' => sh_set($hash, 'thumbnail_url'),
                    'video' => "https://vimeo.com/" . sh_set($hash, 'video_id'),
                    'embed_video' => 'https://player.vimeo.com/video/' . sh_set($hash, 'video_id') . '" frameborder="0" >',
                );
            }
            break;

        case 'youtube':

            $yt_api_key = 'AIzaSyBJhHvfltBxpMIV1tY3vwKK9rO3ms1H4hM';
            preg_match("/v=([^&#]*)/", parse_url($url, PHP_URL_QUERY), $video_id);
            $video_id = $video_id[1];
            $hash = '';
            $content = wp_remote_get('https://www.googleapis.com/youtube/v3/videos?part=snippet&id=' . $video_id . '&key=' . $yt_api_key);
            $hash = json_decode(sh_set($content, 'body'));
            if (!empty(sh_set($hash, 'items'))) {
                $sinppet = sh_set(sh_set(sh_set($hash, 'items'), 0), 'snippet');
                return array(
                    'host' => 'youtube',
                    'provider' => 'YouTube',
                    'title' => sh_set($sinppet, 'title'),
                    'description' => str_replace(array("
", "
", "
"), NULL, sh_set($sinppet, 'description')),
                    'thumbnail' => sh_set(sh_set($sinppet, 'thumbnails'), 'high'),
                    'video' => "http://www.youtube.com/" . sh_set(sh_set(sh_set($hash, 'items'), 0), 'id'),
                    'embed_video' => 'https://www.youtube.com/embed/' . sh_set(sh_set(sh_set($hash, 'items'), 0), 'id') . '" frameborder="0">',
                );
            } else {
                return array(
                    'embed_video' => 'https://www.youtube.com/embed/' . $video_id . '" frameborder="0">',
                );
            }
            break;
        case 'dailymotion':
            preg_match("/video\/([^_]+)/", $url, $video_id);
            $video_id = $video_id[1];
            $content = wp_remote_get("https://api.dailymotion.com/video/$video_id?fields=title,thumbnail_url,owner%2Cdescription%2Cduration%2Cembed_html%2Cembed_url%2Cid%2Crating%2Ctags%2Cviews_total");
            $hash = json_decode(sh_set($content, 'body'));
            if ($hash) {
                return array(
                    'host' => 'dailymotion',
                    'provider' => 'Dailymotion',
                    'title' => $hash->title,
                    'description' => str_replace(array("
", "
", "
"), NULL, $hash->description),
                    'thumbnail' => $hash->thumbnail_url,
                    'embed_video' => $hash->embed_html,
                );
            }
            break;
    }
}

if (get_option('wp_lifeline')) {
    update_option('lifeline', get_option('wp_lifeline'));
    delete_option('wp_lifeline');
}

if ( function_exists( 'vc_map' ) ) {
	function sh_vc_disable_update() {
		if ( function_exists( 'vc_license' ) && function_exists( 'vc_updater' )
		     && ! vc_license()->isActivated()
		) {

			remove_filter( 'upgrader_pre_download',
				[ vc_updater(), 'preUpgradeFilter' ], 10 );
			remove_filter( 'pre_set_site_transient_update_plugins', [
				vc_updater()->updateManager(),
				'check_update'
			] );

		}
	}

	add_action( 'admin_init', 'sh_vc_disable_update', 9 );
}

function charity_shortcode() {
   
// $Posts = query_posts( 'post_type=dict_causes&paged='.$paged); ?>
		<div class="col-md-12">
            <div class="remove-ext">
                <div class="row" > 
				<?php
					
					$args = array( 'post_type' => 'dict_causes','showposts' => 3, 'meta_query' => array( array( 'key' => 'display_on_homepage', 'compare' => '==', 'value' => '1' ) ));
					$the_query = new WP_Query( $args );
					
					 if ( $the_query->have_posts() ): while ( $the_query->have_posts() ): $the_query->the_post(); 
							 $CausesSettings = get_post_meta( get_the_ID(), '_dict_causes_settings', true ); 
							 //$displayhome= get_field('display_on_homepage'); 
							 //if($displayhome = 1):
							 ?>
							 
							<div class="col-md-4">
								<div class="our-cause"> 
									<div class="our-cause-img">
										<?php echo get_the_post_thumbnail( get_the_ID(), '470x318' ); ?>
										<a title="<?php the_title() ?>" href="<?php the_permalink() ?>"><i class="icon-link"></i></a>
									</div>
									<div class="our-cause-detail">
                                        <h3><a title="<?php the_title() ?>" href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
										<span><?php _e( 'In', 'lifeline' ) ?> <a title="" href="<?php the_permalink() ?>"><?php echo sh_set( $CausesSettings, 'location' ); ?></a></span>
										<p><?php echo substr( get_the_content(), 0, 127 ); ?></p>
										<i><?php _e( 'Help Us:', 'lifeline' ); ?> <span><?php echo sh_set( $CausesSettings, 'currency_symbol' ); ?></span> <strong><?php echo sh_set( $CausesSettings, 'donation_needed' ); ?></strong></i>
										<a class="btn-don" data-toggle="modal" data-id="<?php echo get_the_ID() ?>" data-url="<?php echo get_permalink() ?>" data-security="<?php echo wp_create_nonce(LIFELINE_KEY); ?>"
										data-type="post" data-target="#myModal"><?php _e( 'DONATE NOW', 'lifeline' ) ?></a>
										<div id="paypal-button-container"></div>
									</div>
									</a> </div>
							</div>
							<?php
							//endif;
						endwhile;
					endif;
					wp_reset_postdata(); ?>
				</div>
            </div>
        </div>
	<?php				
}
add_shortcode('charity', 'charity_shortcode');