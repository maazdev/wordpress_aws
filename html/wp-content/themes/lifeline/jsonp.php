<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
/*$table = 'transactions';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'doner_name', 'dt' => 0 ),
    array( 'db' => 'type',  'dt' => 1 ),
    array( 'db' => 'organization_id',   'dt' => 2 ),
    array( 'db' => 'organization_name',     'dt' => 3 ),
    array(
        'db'        => 'date',
        'dt'        => 4
    ),
    array(
        'db'        => 'amount',
        'dt'        => 5,
        'formatter' => function( $d, $row ) {
            return '$'.number_format($d);
        }
    )
);
 
$sql_details = array(
    'user' => 'root',
    'pass' => 'inv12345',
    'db'   => 'creativefund',
    'host' => 'localhost'
);
 
 */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
 /*
require( 'ssp.class.php' );
 
// Validate the JSONP to make use it is an okay Javascript function to execute
$jsonp = preg_match('/^[$A-Z_][0-9A-Z_$]*$/i', $_GET['callback']) ?
    $_GET['callback'] :
    false;
 
if ( $jsonp ) {
    echo $jsonp.'('.json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    ).');';
}

*/ 

$servername = "localhost";
$username = "root";
$password = "inv12345";
$db = "creativefund";

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//echo "Connected successfully";

$sql = 'SELECT * from transactions';
$rows = array();
$result = $conn->query($sql);



if ($result->num_rows > 0) {
    // output data of each row
	
    while($r = $result->fetch_assoc()) {
		$rows[] = $r;
}
echo json_encode(["data"=>$rows]);
//print json_encode($rows);
} 
$conn->close();

?>