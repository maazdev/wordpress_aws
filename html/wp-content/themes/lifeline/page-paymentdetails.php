<?php /* Template Name: PaymentDetail*/ ?>
 
<?php get_header(); ?>
<?php
$servername = "localhost";
$username = "root";
$password = "inv12345";
$dbname = "creativefund";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

?>
<div class="container pay-details">
		<?php
		$sql = "SELECT SUM(amount) AS Totalamount FROM transactions";
		//var_dump($sql);
		$res = mysqli_query($conn, $sql);
		//var_dump($res);
		$row = mysqli_fetch_assoc($res); 
		//echo "TotalAmount" . $row['Totalamount'];
		$totalamount = $row['Totalamount'];

		$sql = "SELECT SUM(amount) AS Totalamountstripe FROM transactions where type='stripe'";
		//var_dump($sql);
		$res = mysqli_query($conn, $sql);
		//var_dump($res);
		$row = mysqli_fetch_assoc($res); 
		//echo "TotalAmountStripe" . $row['Totalamountstripe'];
		$totalstripe = $row['Totalamountstripe'];

		$sql = "SELECT SUM(amount) AS Totalamountpaypal FROM transactions where type='paypal'";
		//var_dump($sql);
		$res = mysqli_query($conn, $sql);
		//var_dump($res);
		$row = mysqli_fetch_assoc($res); 
		//echo "TotalAmountPaypal" . $row['Totalamountpaypal'];
		$totalpaypal = $row['Totalamountpaypal'];
		?>

		<div class="row">
				<div class="col-sm-6"><div class="total-amount"><h3>Paypal Amount</h3><?php echo '$'.$totalpaypal ?></div></div>
				<div class="col-sm-6"><div class="total-amount total-stripe"><h3>Stripe Amount</h3><?php echo '$'.$totalstripe ?></div></div>
		</div>
		<div class="col-sm-6 total-amount col-sm-offset-3 total-pay"><h3>Total Amount</h3><?php echo '$'.$totalamount ?></div>
		<div style="clear:both">
 
 
<div class="filters">
<?php
$sql = "SELECT * from type";
//var_dump($sql);
$res = mysqli_query($conn, $sql);
//var_dump($res);


			echo '<div class="col-sm-3">';
			echo '<select id="type" name="typeId" onchange="loaddata();">';
			echo '<option selected="true" disabled="disabled" value="">Select type</option>';
			while ($row = mysqli_fetch_array($res)) {
				echo "<option value='" . $row['type_id'] ."'>" . $row['type'] ."</option>";
			} 			
			echo '</select>'; 
			echo '</div>';
			$args = array( 'post_type' => 'dict_causes','posts_per_page' => -1);
					$query = new WP_Query($args);
					echo '<div class="col-sm-3">';
							echo '<select id="org" name="orgId" onchange="loaddata1();">';
							echo '<option selected="true" disabled="disabled" value="">Select organization</option>';
							if ($query->have_posts()):while ($query->have_posts()):$query->the_post();?>
								<option value="<?php echo get_the_ID() ?>"><?php echo the_title() ?></option>
							<?php
							endwhile;
							endif;
							wp_reset_postdata();
					echo '</select>'; 
					echo '</div>';
					?>
		<div class="col-sm-6 input-daterange">
				<div class="col-md-4">  
					<input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                </div>  
                <div class="col-md-4">  
                     <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                </div>  
                <div class="col-md-4">  
                     <input type="button" name="filter" id="filter" value="Filter" class="btn btn-info" />  
                </div>  
                <div style="clear:both"></div>    
		</div>

 
</div>

<?php
	//$sql = "SELECT organization_name,amount FROM transactions inner join wp_post on trans.organization_id = org.ID";
		//var_dump($sql);
		//$res = mysqli_query($conn, $sql);
		/* $res= mysqli_query($conn,"SELECT organization_id,organization_name,sum(amount) as TotalAmount FROM transactions group by organization_id"); 
		
		//var_dump($res);
		 while($data = mysqli_fetch_array($res))
        {
			
			echo $data['organization_name'];
			echo $data['TotalAmount'];
        } 
 */
?>

<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
				<th>transaction Id</th>
				<th>date</th>
                <th>Doner Name</th>
				<th>organization name</th>
				<th>type</th>
                <th>amount</th>  
              
            </tr>
        </thead>
        <tfoot>
            <tr>
				<th>transaction Id</th>
				<th>date</th>
                <th>Doner Name</th>
				<th>organization name</th>
				<th>type</th>
                <th>amount</th>  
            </tr>
        </tfoot>
    </table>

	</div>
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
 <!-- <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script> -->
	<script>
		$(document).ready(function() {
		$('#example').DataTable( {
        "ajax": "http://54.190.61.5/wp-content/themes/lifeline/jsonp.php",
        "columns": [
			{ "data": "id" },
			 { "data": "date" },
            { "data": "doner_name" },
			 { "data": "organization_name" },
			 { "data": "type" },
            { "data": "amount" , render: $.fn.dataTable.render.number( ',', '.', 0, '$' )}
        ]
    } );
} );

function loaddata(){
	var type = $('#type').find('option:selected').val();
	console.log(type);
	/* $('#example').remove();
	var table = $('#example').DataTable();
	console.log(table);
	
	    $('#example').DataTable( {
        "ajax": "http://54.190.61.5/wp-content/themes/lifeline/type_ajax.php",
        "columns": [
			{ "data": "id" },
			 { "data": "date" },
            { "data": "doner_name" },
			 { "data": "organization_name" },
			 { "data": "type" },
            { "data": "amount" , render: $.fn.dataTable.render.number( ',', '.', 0, '$' )}
        ]
    } ); */
	 $('#example').DataTable().destroy();
	var dataTable = $('#example').DataTable({
   "processing" : true,
   "serverSide" : true,
   "order":[],
   "ajax" : {
    url:"http://54.190.61.5/wp-content/themes/lifeline/type_ajax.php",
    type:"POST",
    data:{'typeId':type}
   }
  });
}


function loaddata1()
{
 var org = $('#org').find('option:selected').val();
	
  $('#example').DataTable().destroy();
	var dataTable = $('#example').DataTable({
   "processing" : true,
   "serverSide" : true,
   "order":[],
   "ajax" : {
    url:"http://54.190.61.5/wp-content/themes/lifeline/org_ajax.php",
    type:"POST",
    data:{'orgId':org}
   }
  });
}
	</script>
     
	<script> 
	
      $(document).ready(function(){

		$('#from_date').datepicker({
		  todayBtn:'linked',
		  format: "yyyy-mm-dd",
		  autoclose: true
		 });
		 
		$('#to_date').datepicker({
		  todayBtn:'linked',
		  format: "yyyy-mm-dd",
		  autoclose: true
		 });		 
           /* $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });   
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  */ 
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date != '' && to_date != '')  
                {  
                    /*  $.ajax({  
                          url:"http://54.190.61.5/wp-content/themes/lifeline/ajax_date.php",  
                          method:"POST",  
                          data:{'from_date':from_date, 'to_date':to_date},  
                          success:function(data)  
                          {  
                            $('#example').html(data);  
                          }  
                     }); */

					 $('#example').DataTable().destroy();
						var dataTable = $('#example').DataTable({
					   "processing" : true,
					   "serverSide" : true,
					   "order":[],
					   "ajax" : {
						url:"http://54.190.61.5/wp-content/themes/lifeline/ajax_date.php",
						type:"POST",
						data:{'from_date':from_date, 'to_date':to_date}
					   }
					  });
                }  
                else  
                {  
                  alert("Please Select Date");  
                }  
           }); 
/* 
 fetch_data('no');

 function fetch_data(is_date_search, start_date='', end_date='')
 {
  var dataTable = $('#example').DataTable({
   "processing" : true,
   "serverSide" : true,
   "order" : [],
   "ajax" : {
    url:"http://localhost/creativefund/wp-content/themes/lifeline/ajax_date.php",
    type:"POST",
    data:{
     is_date_search:is_date_search, start_date:start_date, end_date:end_date
    }
   }
  });
 }

 $('#filter').click(function(){
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  if(start_date != '' && end_date !='')
  {
   $('#order_data').DataTable().destroy();
   fetch_data('yes', start_date, end_date);
  }
  else
  {
   alert("Both Date is Required");
  }
 });  */
 
});

		   
      
 </script>

<?php get_footer(); ?>