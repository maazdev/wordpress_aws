<?php
sh_custom_header();
$Settings = get_option('lifeline');
$PostSettings = get_post_meta(get_the_ID(), '_' . sh_set($post, 'post_type') . '_settings', true);
$attachments = explode(',', sh_set($PostSettings, 'gallery'));
$sidebar = sh_set($PostSettings, 'sidebar') ? sh_set($PostSettings, 'sidebar') : '';
$col_class = sh_set($PostSettings, 'sidebar') ? 'col-md-9' : 'col-md-12';
$paypal = $GLOBALS['_sh_base']->donation;
$percent = (sh_set($PostSettings, 'donation_needed')) ? (int) str_replace(',', '', sh_set($PostSettings, 'donation_collected')) / (int) str_replace(',', '', sh_set($PostSettings, 'donation_needed')) : 0;
$donation_percentage = round($percent * 100, 2);
$symbol = (sh_set($PostSettings, 'currency_symbol')) ? sh_set($PostSettings, 'currency_symbol') : '$';
$sh_currency_code = (sh_set($PostSettings, 'currency_code')) ? sh_set($PostSettings, 'currency_code') : 'USD';
$_SESSION['sh_causes_id'] = get_the_ID();
$_SESSION['sh_causes_url'] = get_permalink();
$_SESSION['sh_causes_page'] = true;
$_SESSION['sh_currency_code'] = $sh_currency_code;
$_SESSION['sh_donation_needed'] = sh_set($PostSettings, 'donation_needed');
$_SESSION['sh_donation_collected'] = sh_set($PostSettings, 'donation_collected');
$_SESSION['sh_currency_symbol'] = $symbol;
$_SESSION['sh_post_type'] = 'causes';
$paypal_res = '';

if (isset($_GET['recurring_pp_return']) && $_GET['recurring_pp_return'] == 'return') {
    if(defined('CPATH'))
        $paypal_res = require_once(CPATH. 'includes/pp_recurring/order_confirm.php');
}
if ($notif = $paypal->_paypal->handleNotification())
    $paypal_res = $paypal->single_pament_result($notif);
?>
<!-- <div class="top-image"><img src="http://54.190.61.5/wp-content/uploads/17.jpg" alt="" /></div> -->
<!-- Page Top Image -->
<section class="inner-page <?php echo ( sh_set($PostSettings, 'sidebar_pos') == 'left' ) ? ' switch' : ''; ?>">
    <div class="container">
        <div class="row">
	        <?php if (sh_set($PostSettings, 'sidebar_pos') == 'left') : ?>
                <div class="sidebar col-md-3 pull-left">
			        <?php dynamic_sidebar($sidebar); ?>
                </div>
	        <?php endif; ?>
            <div class="col-md-12">
                <?php if (have_posts()): while (have_posts()): the_post(); ?>
                        <div class="post">


                            <?php if( sh_set($PostSettings, 'show_donation_bar') == "true" ) : ?>
                            <div class="cause-bar">
                                <div class="cause-box"><h3><span><?php echo $symbol; ?></span><?php echo sh_set($PostSettings, 'donation_needed'); ?></h3><i><?php _e('NEEDED DONATION', 'lifeline'); ?></i></div>
                                <div class="cause-progress">
                                    <div class="progress-report">
                                        <h6><?php _e('PHASES', 'lifeline') ?></h6>
                                        <span><?php echo $donation_percentage ?>%</span>
                                        <div class="progress pattern">
                                            <div class="progress-bar" style="width: <?php echo $donation_percentage ?>%"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cause-box"><h3><span><?php echo $symbol; ?></span><?php echo sh_set($PostSettings, 'donation_collected'); ?></h3><i><?php _e('COLLECTED DONATION', 'lifeline') ?></i></div>
                                <a data-url="<?php echo get_permalink() ?>" data-type="post" data-id="<?php echo get_the_ID() ?>" class="header-btn" href="http://54.190.61.5/for-charities/" title="">DONATE NOW</a>      </div>
                            <?php endif; ?>
                              <div class="inner-causes-header">
                                <div class="container">
                                <?php $logo = get_field('organization_logo'); ?>
                                <div class="col-sm-9"><div class="org-logo"><img src="<?php echo $logo['url']; ?>" alt="" width="100px" height="100px"></div><div class="cause-content"><h1><?php the_title(); ?> </h1></div></div>
                                 <div class="col-sm-3">
                                 <div style="background-color:#4fc0aa;" class="cause-box donate-drop-btn" data-url="<?php echo get_permalink() ?>" data-type="post" data-id="<?php echo get_the_ID() ?>"><h4><?php _e('DONATE NOW', 'lifeline'); ?></h4></div>
                             </div>
                               </div>

</div>



                            <div class= "post-desc">

                                <?php the_content(); ?>
                            </div>

                            <div class="cloud-tags">
                                <?php the_tags('<h3 class="sub-head">' . __('Tags Clouds', 'lifeline') . '</h3>', ''); ?>
                            </div><!-- Tags -->
                            <?php
                            if (is_single() && comments_open())
                                comments_template();
                            ?>

                        </div>
                        <?php
                    endwhile;
                endif;
                ?>

                  </div>
	        <?php if (sh_set($PostSettings, 'sidebar_pos') == 'right') : ?>
                <div class="sidebar col-md-3 pull-right">
                    <?php dynamic_sidebar($sidebar); ?>
                </div>
            <?php endif; ?>

        </div>
    </div>

</section>


<?php get_footer(); ?>
