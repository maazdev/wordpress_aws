<?php
/* Template Name: Contact Page */
sh_custom_header();
$settings = get_post_meta( get_the_ID(), '_page_settings', true ); //printr($settings);
$sidebar = sh_set( $settings, 'sidebar' );
$paged = get_query_var( 'paged' );
$theme_options = get_option( 'lifeline' );
?> 
<?php if(sh_set( $settings, 'top_image' )) : ?>
	<div class="top-image"> <img src="<?php echo sh_set( $settings, 'top_image' ); ?>" alt="" /> </div>      
<?php endif; ?>            
<section class="inner-page">

	<div class="container">
		 <?php if(sh_set( $settings, 'show_page_title' ) == 1  ) :

			$title = sh_set( $settings, 'page_title' ); 
			//print_r($title); exit('aaa');
			if($title) :
					$title = $title;
			else: 
					$title = get_the_title();
			endif;  ?>
		<div class="page-title">
           <?php 
			$con = explode( ' ', $title, 2 ); ?>
            <?php echo '<h1>' . sh_set( $con, '0' ) . ' <span>' . sh_set( $con, '1' ) . '</span></h1>' ?>
        </div>
    <?php endif; ?>
        <div class="row">
			<!-- <div class="col-md-6">
				<div class="contact-info">
					<h3 class="sub-head"><?php _e( 'CONTACT INFORMATION', 'lifeline' ); ?></h3>

					<?php echo stripslashes( sh_set( $theme_options, 'google_map_code' ) ); ?>
					<p><?php echo sh_set( $theme_options, 'contact_page_text' ); ?></p>
					<ul class="contact-details">
                        <?php if(sh_set($theme_options, 'contact_page_address') !=''): ?>
						<li>
							<span><i class="icon-home"></i><?php _e( 'ADDRESS', 'lifeline' ); ?></span>
							<p><?php echo sh_set( $theme_options, 'contact_page_address' ); ?></p>
						</li>
                        <?php endif; ?>
						<?php if(sh_set($theme_options, 'contact_page_phone') !=''): ?>
						<li>
							<span><i class="icon-phone-sign"></i><?php _e( 'PHONE NO', 'lifeline' ); ?></span>
							<p><a href="<?php echo esc_attr( sh_set( $theme_options, 'contact_page_phone_link' ) ); ?>" title=""><?php echo sh_set( $theme_options, 'contact_page_phone' ); ?></a></p>
						</li>
						<?php endif; ?>
						<?php if(sh_set($theme_options, 'contact_page_email') !=''): ?>
						<li>
							<span><i class="icon-envelope-alt"></i><?php _e( 'EMAIL ID', 'lifeline' ); ?></span>
							<p><a href="<?php echo esc_attr( sh_set( $theme_options, 'contact_page_email_link' ) ); ?>" title=""><?php echo sh_set( $theme_options, 'contact_page_email' ); ?></a></p>
						</li>
						<?php endif; ?>
						<?php if(sh_set($theme_options, 'contact_page_website') !=''): ?>
						<li>
							<span><i class="icon-link"></i><?php _e( 'WEB ADDRESS', 'lifeline' ); ?></span>
							<p><?php echo sh_set( $theme_options, 'contact_page_website' ); ?></p>
						</li>
                        <?php endif; ?>
					</ul>
				</div>
			</div> -->	<!-- Contact Info -->
			<div class="col-md-8 contact-head">

				<div id="msgs2"></div>
				<div class="form">
					<h3 class="sub-head"><?php _e( 'CONTACT US BY MESSAGE', 'lifeline' ); ?></h3>
					<p><?php _e( 'Your email address will not be published. Required fields are marked', 'lifeline' ); ?> <span>*</span></p>
					<form method="post"  action="<?php echo admin_url( 'admin-ajax.php?action=dictate_ajax_callback&subaction=sh_contact_form_submit' ); ?>" name="contactform" id="lifeline_contact_form1">

                        <div class="msgs"></div>
                        <input type="hidden"  id="lifeline_securitykey" name="lifeline_securitykey" value="<?php echo wp_create_nonce(LIFELINE_KEY); ?>">
                        <label for="name" accesskey="U"><?php _e( 'Full name', 'lifeline' ); ?> <span>*</span></label>
						<input name="contact_name" class="form-control input-field" type="text" id="name" size="30" value="" />
						<label for="email" accesskey="E"><?php _e( 'Email Address', 'lifeline' ); ?> <span>*</span></label>

						<input name="contact_email" class="form-control input-field" type="text" id="email" size="30" value="" />
						<label for="comments" accesskey="C"> <?php _e( 'Message', 'lifeline' ); ?><span>*</span></label>
						<textarea name="contact_message" rows="9" id="comments" rows="7" class="form-control input-field"></textarea>

						<?php if ( sh_set( $theme_options, 'captcha_status' ) == 'true' ): ?>
							<script type="text/javascript">
	                            var RecaptchaOptions = {
	                                theme: 'clean'
	                            };
							</script>
							<?php echo recaptcha_get_html( sh_set( $theme_options, 'captcha_api' ) ); ?>
						<?php endif; ?>

						<input type="submit" class="form-button submit" id="submit2" value="<?php _e( 'SEND MESSAGE', 'lifeline' ); ?>" />

					</form>
					<div id="admn_url" style="display:none"><?php echo get_template_directory_uri(); ?></div>
				</div>
                <script>
                    jQuery(document).ready(function ($) {

                        $('#lifeline_contact_form1').live('submit', function (e) {
                            e.preventDefault();
                            var thisform = this;
                            var fields = $(this).serialize();
                            var url = $(this).attr('action');
                            var url2 = document.getElementById('admn_url').innerHTML;
                            $("#msgs2").slideUp(750);
                            $('#msgs2').hide();
                            $('#submit2')
                                    .after('<img class="loader" src=' + url + '/images/ajax-loader.gif  />').attr("disabled", "disabled");

                            $.ajax({
                                url: url,
                                type: 'POST',
                                data: fields,
                                success: function (data) {
                                    document.getElementById('msgs2').innerHTML = data;
                                    $('#msgs2').slideDown('slow');
                                    $('#lifeline_contact_form1 img.loader').fadeOut('slow', function () {
                                        $(this).remove()
                                    });
                                    $('#submit2').removeAttr('disabled');
                                    if (data.match('success') != null)
                                        $('#lifeline_contact_form1').slideUp('slow');
                                    if (data.match('success') != null)
                                        $('.form h3').slideUp('slow');
                                    if (data.match('success') != null)
                                        $('.form p').slideUp('slow');

                                }
                            });
                            return false;
                        });
                    });
				</script>
			</div>	<!-- Message Form -->
		</div>	
	</div>
	<?php if ( sh_set( $theme_options, 'contact_text_status' ) == 'true' ) : ?>
	    <div class="social-connect">	
			<div class="container">
				<h3><?php echo sh_set( $theme_options, 'social_section_title' ); ?></h3>
				<ul class="social-bar">
					<?php echo ( sh_set($theme_options, 'contact_rss') ) ? '<li><a target="_blank" href="' . esc_url(sh_set($theme_options, 'contact_rss')) . '" title=""><img alt="" src="'. get_template_directory_uri().'/images/rss.jpg"></a></li>' : ''; ?>
					<?php echo ( sh_set($theme_options, 'contact_gplus') ) ? '<li><a target="_blank" href="' . esc_url(sh_set($theme_options, 'contact_gplus')) . '" title=""><img alt="" src="'. get_template_directory_uri().'/images/gplus.jpg"></a></li>' : ''; ?>
					<?php echo ( sh_set($theme_options, 'contact_facebook') ) ? '<li><a target="_blank" href="' . esc_url(sh_set($theme_options, 'contact_facebook') ). '" title=""><img alt="" src="'. get_template_directory_uri().'/images/facebook.jpg"></a></li>' : ''; ?>
					<?php echo ( sh_set($theme_options, 'contact_twitter') ) ? '<li><a target="_blank" href="' . esc_url(sh_set($theme_options, 'contact_twitter')) . '" title=""><img alt="" src="'. get_template_directory_uri().'/images/twitter-icon.png"></a></li>' : ''; ?>
					<?php echo ( sh_set($theme_options, 'contact_linkedin') ) ? '<li><a target="_blank" href="' . esc_url(sh_set($theme_options, 'contact_linkedin')) . '" title=""><img alt="" src="'. get_template_directory_uri().'/images/linked-in.jpg"></a></li>' : ''; ?>
					<?php echo ( sh_set($theme_options, 'contact_pintrest') ) ? '<li><a target="_blank" href="' . esc_url(sh_set($theme_options, 'contact_pintrest')) . '" title=""><img alt="" src="'. get_template_directory_uri().'/images/pinterest.jpg"></a></li>' : ''; ?>
					<?php echo ( sh_set($theme_options, 'contact_instagram') ) ? '<li><a target="_blank" href="' . esc_url(sh_set($theme_options, 'contact_instagram')) . '" title=""><img alt="" src="'. get_template_directory_uri().'/images/insta.jpg"></a></li>' : ''; ?>
					<?php echo ( sh_set($theme_options, 'contact_youtube') ) ? '<li><a target="_blank" href="' . esc_url(sh_set($theme_options, 'contact_youtube')) . '" title=""><img alt="" src="'. get_template_directory_uri().'/images/youtube.jpg"></a></li>' : ''; ?>
                </ul>
			</div>
		</div>
	<?php endif; ?>
    <!-- Social Media Bar -->
    <?php if ( sh_set( $theme_options, 'contact_text_counter' ) == 'true' ) : ?>
		<section>
			<div class="work-section block">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="working">
								<h3 class="sub-head"><?php echo sh_set( $theme_options, 'country_section_title' ); ?></h3>
								<p><?php echo sh_set( $theme_options, 'country_section_text' ); ?></p>
							</div>
						</div>
						<?php
						$countries = sh_set( $theme_options, 'contact_countries' );
						unset( $countries['%s'] );
						if ( $countries ) {
							$chunks = array_chunk( $countries, 4 );
							?>
							<div class="col-md-6">
								<div class="row">
									<div class="countries">
										<ul class="slides">
											<?php foreach ( $chunks as $chunk ) : ?>
												<li>
													<?php foreach ( $chunk as $c ):
														?>
														<div class="col-md-3">
															<img width="97" height="50" src="<?php echo sh_set( $c, 'contact_country_img' ); ?>" alt="" />
														</div>
													<?php endforeach; ?>
												</li>
											<?php endforeach; ?>

										</ul>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section><!-- Working -->

		<script>
	        jQuery(document).ready(function ($) {
	            $('.countries').flexslider({
	                animation: "slide",
	                animationLoop: false,
	                slideShow: false,
	                controlNav: false,
	                pausePlay: false,
	                mousewheel: false,
	                start: function (slider) {
	                    $('body').removeClass('loading');
	                }
	            });
	        });
		</script>  
	<?php endif; ?> 
	<?php //echo do_shortcode('[sh_social_media]');  ?><!-- Social Media Bar -->

	<?php //echo do_shortcode('[sh_countries_slider]');  ?>

</section>

<?php get_footer(); ?>
